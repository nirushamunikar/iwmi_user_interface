const detail = {
  path: "/Detail",
  name: "Detail Plan",
  component: () => import("@/views/shared/Layout.vue"),
  children: [
    {
      path: "/Detail/additionalInfo",
      name: "Create Detail Planning",

      component: () => import("@/views/shared/PlanLayout.vue"),
      children: [
        {
          path: "/Detail/additionalInfo",
          name: "additionalInfo",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () =>
            import("@/views/DetailPlan/AdditionalInformation.vue")
        },
        {
          path: "/Detail/SelectedProblems",
          name: "SelectedProblems",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/DetailPlan/SelectedProblemList.vue")
        },
        {
          path: "/Detail/ActivityList",
          name: "ActivityList",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/DetailPlan/ActivityList.vue")
        },
        {
          path: "/Detail/ActivityStakeholder",
          name: "ActivityStakeholder",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () =>
            import("@/views/Plan/Detailed/ActivityStakeholder.vue")
        },
        {
          path: "/Detail/ScopeOfActivityDetail",
          name: "ScopeOfActivityDetail",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () =>
            import("@/views/Plan/Detailed/ScopeOfActivityDetail.vue")
        },
        {
          path: "/Detail/ScopeOfActivityDetailList",
          name: "ScopeOfActivityDetailList",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () =>
            import("@/views/Plan/Detailed/Component/ScopeOfActivityDetailList.vue")
        },
        {
          path: "/Detail/Overview",
          name: "Overview",
          component: () => import("@/views/DetailPlan/SituationOverview.vue")
        },
        {
          path: "/Reporting/WorkPlan",
          name: "WorkPlan",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Reporting/WorkPlanPreparation.vue")
        },
        {
          path: "/Reporting/Report",
          name: "Report",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Reporting/Report.vue")
        },
        {
          path: "/Reporting/ActivityList",
          name: "MonitoringActivityList",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Reporting/ActivityList.vue")
        },
        {
          path: "/Reporting/Evaluation",
          name: "Evaluation",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () =>
            import("@/views/Reporting/MonitoringAndEvaluation.vue")
        },
        {
          path: "/Reporting/ActivityProfile",
          name: "ActivityProfile",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Reporting/ActivityProfile.vue")
        }
      ]
    }
  ]
};

export default detail;
