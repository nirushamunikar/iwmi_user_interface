const planroute = {
  path: "/Plan",
  name: "Plan",
  component: () => import("@/views/shared/Layout.vue"),
  children: [
    {
      path: "/Plan/Initial",
      name: "CreatePlanning",
      meta: {
        needsAuth: true,
        accesscheck: true
      },
      component: () => import("@/views/Plan/Beforestep/CreatePlan.vue")
    },
    {
      path: "/Plan/List",
      name: "Planning",
      meta: {
        needsAuth: true
      },
      component: () => import("@/views/shared/PlanLayout.vue"),
      children: [
        {
          path: "/Plan/List",
          name: "Plan Listing",
          meta: {
            needsAuth: true
          },
          component: () => import("@/views/Plan/Beforestep/PlanListing.vue")
        },
        {
          path: "/Plan/Problem",
          name: "Problem",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Plan/Step2/Problem.vue")
        },
        {
          path: "/Plan/Problem/list",
          name: "Problem List",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Plan/Step2/ProblemList.vue")
        },
        {
          path: "/Plan/CreatePlan",
          name: "CreatePlan",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Plan/Beforestep/CreatePlan.vue")
        },
        {
          path: "/Plan/Step1/BioPhysical",
          name: "BioPhysical",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Plan/Step1/BioPhysical.vue")
        },
        {
          path: "/Plan/Step1/Stakeholders",
          name: "Stakeholders",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Plan/Step1/Stakeholders.vue")
        },
        {
          path: "/Plan/Step1/SocioEconomic",
          name: "SocioEconomic",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Plan/Step1/SocioEconomic.vue")
        },
        {
          path: "/Plan/Problem/ProblemRank",
          name: "problem Rank",
          meta: {
            needsAuth: true,
            accesscheck: true
          },
          component: () => import("@/views/Plan/Step3/ProblemRank.vue")
        }
      ]
    }
  ]
};

export default planroute;
