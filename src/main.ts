import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import locale from "element-ui/lib/locale/lang/en";
import Snotify, { SnotifyPosition } from "vue-snotify";
import { SnotifyService } from "vue-snotify/SnotifyService";
import "vue-snotify/styles/material.css";
import VeeValidate from "vee-validate";
import VueProgressBar from "vue-progressbar";
import JWTStore from "./store/modules/jwt";
import API from "@/store/API/jwt";

Vue.use(VueProgressBar, {
  color: "#41B883",
  failedColor: "red",
  height: "2px",
  thickness: "3px",
  autoRevert: true,
  autoFinish: false
});
const options = {
  toast: {
    position: SnotifyPosition.leftBottom
  }
};
Vue.use(Snotify, options);
Vue.use(ElementUI, { locale });
Vue.use(VeeValidate);
Vue.config.productionTip = false;
/*
Vue.config.errorHandler = function (err, vm, info) {
  console.log(err);
  console.log(vm);
  console.log(info);
  console.log('[Global Error Handler]: Error in ' + info + ': ' + err);
};
*/
declare module "vue/types/vue" {
  interface Vue {
    $snotify: SnotifyService;
  }
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

router.beforeResolve(async (to, from, next) => {
  if (to.matched.some(record => record.meta.needsAuth)) {
    if (
      JWTStore.loggedIn &&
      API.verifyAccessToken({ token: localStorage.getItem("access_token") })
    ) {
      if (
        await API.checkAccess({
          planid: Number(to.query.planid),
          component: String(to.name),
          accesscheck: to.meta.accesscheck
        })
      ) {
        next();
      } else {
        next("/plan");
      }
    } else if (!JWTStore.loggedIn) {
      next({
        path: "/"
      });
    }
  } else {
    next();
  }
});
