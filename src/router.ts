import Vue from "vue";
import Router from "vue-router";
import planroute from "@/router/plan";
import detailroute from "@/router/detailPlan";
import JWTStore from "@/store/modules/jwt";
Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    detailroute,
    {
      path: "/",
      name: "login",
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem("access_token")) {
          next({ path: "/plan/list" });
        } else {
          next();
        }
      },
      component: () =>
        import(/* webpackChunkName: "about" */ "@/views/Authscreen/login.vue")
    },
    // {
    //   path: "/plan",
    //   meta: {
    //     needsAuth: true
    //   },
    //   beforeEnter: (to, from, next) => {
    //     if (to.matched.some(record => record.meta.needsAuth)) {
    //       if (JWTStore.loggedIn) {
    //         next();
    //       } else {
    //         next({
    //           path: "/"
    //         });
    //       }
    //     } else {
    //       next();
    //     }
    //   },
    //   component: () =>
    //     import(
    //       /* webpackChunkName: "about" */ "@/views/Landing/Landingscreen.vue"
    //     )
    // },
    planroute,
    {
      path: "/register",
      meta: {
        needsAuth: true
      },
      beforeEnter: (to, from, next) => {
        if (to.matched.some(record => record.meta.needsAuth)) {
          if (JWTStore.loggedIn && localStorage.getItem("role") === "Admin") {
            next();
          } else {
            next({
              path: "/"
            });
          }
        } else {
          next();
        }
      },
      component: () =>
        import(
          /* webpackChunkName: "about" */ "@/views/Authscreen/register.vue"
        )
    },
    {
      path: "/forgotpassword",
      name: "forgotpassword",
      component: () =>
        import(
          /* webpackChunkName: "about" */ "@/views/Authscreen/forgotpassword.vue"
        )
    },
    {
      path: "/resetpassword/:uid/:token",
      name: "resetpassword",
      component: () =>
        import(
          /* webpackChunkName: "about" */ "@/views/Authscreen/resetpassword.vue"
        )
    },
    {
      path: "/changesuccess",
      name: "changesuccess",
      component: () =>
        import(
          /* webpackChunkName: "about" */ "@/views/Authscreen/changesuccess.vue"
        )
    },
    {
      path: "/Admin",
      name: "Admin",
      component: () => import("@/views/shared/AdminLayout.vue"),
      children: [
        // {
        //   path: "/register",
        //   meta: {
        //     needsAuth: true
        //   },
        //   beforeEnter: (to, from, next) => {
        //     if (to.matched.some(record => record.meta.needsAuth)) {
        //       if (
        //         JWTStore.loggedIn &&
        //         localStorage.getItem("role") === "Admin"
        //       ) {
        //         next();
        //       } else {
        //         next({
        //           path: "/"
        //         });
        //       }
        //     } else {
        //       next();
        //     }
        //   },
        //   component: () =>
        //     import(
        //       /* webpackChunkName: "about" */ "@/views/Authscreen/register.vue"
        //     )
        // },
        {
          path: "/Admin/Settings",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (
                JWTStore.loggedIn &&
                localStorage.getItem("role") === "Admin"
              ) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(/* webpackChunkName: "about" */ "@/views/Admin/Settings.vue")
        },
        {
          path: "/Admin/Role",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (
                JWTStore.loggedIn &&
                localStorage.getItem("role") === "Admin"
              ) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Authscreen/role.vue"
            )
        },
        {
          path: "/Admin/UserList",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (
                JWTStore.loggedIn &&
                localStorage.getItem("role") === "Admin"
              ) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Admin/UserSettings.vue"
            )
        },
        {
          path: "/Admin/Layer",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (
                JWTStore.loggedIn &&
                localStorage.getItem("role") === "Admin"
              ) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Admin/LayerSettings.vue"
            )
        },
        {
          path: "/Admin/Program",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (
                JWTStore.loggedIn &&
                localStorage.getItem("role") === "Admin"
              ) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Admin/ProgramSettings.vue"
            )
        },
        {
          path: "/Admin/Activity",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (
                JWTStore.loggedIn &&
                localStorage.getItem("role") === "Admin"
              ) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Admin/ActivitySettings.vue"
            )
        },
        {
          path: "/Admin/SOA",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (
                JWTStore.loggedIn &&
                localStorage.getItem("role") === "Admin"
              ) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Admin/ScopeOfActivitySettings.vue"
            )
        },
        {
          path: "/Admin/Objective",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (
                JWTStore.loggedIn &&
                localStorage.getItem("role") === "Admin"
              ) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Admin/ObjectiveSettings.vue"
            )
        },
        {
          path: "/User/Profile",
          name: "profile",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (JWTStore.loggedIn) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Authscreen/profile.vue"
            )
        },
        {
          path: "/changeusername",
          name: "changeusername",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (JWTStore.loggedIn) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Authscreen/changeusername.vue"
            )
        },
        {
          path: "/changepassword",
          name: "changepassword",
          meta: {
            needsAuth: true
          },
          beforeEnter: (to, from, next) => {
            if (to.matched.some(record => record.meta.needsAuth)) {
              if (JWTStore.loggedIn) {
                next();
              } else {
                next({
                  path: "/"
                });
              }
            } else {
              next();
            }
          },
          component: () =>
            import(
              /* webpackChunkName: "about" */ "@/views/Authscreen/changepassword.vue"
            )
        },
        {
          path: "/About",
          meta: {
            needsAuth: true
          },
          component: () =>
            import(/* webpackChunkName: "about" */ "@/views/Landing/About.vue")
        }
      ]
    },
    {
      path: "*",
      component: () =>
        import(
          /* webpackChunkName: "about" */ "@/views/Errorscreen/notfound.vue"
        )
    }
  ]
});
