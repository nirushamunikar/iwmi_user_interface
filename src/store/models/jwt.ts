import { District, Province } from "@/store/models/meta";

export interface userInfo {
  username: string;
  password: string;
}

export interface userCreate {
  username: string;
  first_name: string;
  last_name: string;
  designation: string;
  email: string;
  password: string;
  repeatpassword: string;
}

export interface userInRole {
  id?: number;
  user: number;
  role: number;
  district: [];
  username?: string;
  rolename?: string;
  provinceid?: number;
  p?: Province[];
  d?: District[];
  province_district?: any;
  selected?: boolean;
}

export interface Token {
  access: string;
  refresh: string;
}

export interface userDetail {
  username: string;
  first_name: string;
  last_name: string;
  email: string;
}

export interface changeUsername {
  current_password: string;
  new_username: string;
  re_new_username: string;
}

export interface changePassword {
  current_password: string;
  new_password: string;
  re_new_password: string;
}

export interface forgotPassword {
  email: string;
}

export interface resetPassword {
  uid: string;
  token: string;
  new_password: string;
  re_new_password: string;
}
