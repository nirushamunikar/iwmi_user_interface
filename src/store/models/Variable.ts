export interface UserSubmit {
    UserName: string;
    Password: string;
    DeviceType: string
}

export interface UserInfo {
    UserName: string;
    UserId: number
}

export interface District {
    id: number;
    name: string
}