export interface WatershedPlan {
  id: number;
  estimatedbudget: string;
  selectionreason: string;
  objectives: string;
  scopes: string;
  name: string;
  priorityrank: number;
  priority: string;
  fromyear: string;
  toyear: string;
  total_plan_year: number;
  watershedname: string;
  watershedid: number;
  subwatershedname: string;
  subwatershedid: number;
  biophysicallist: BioPhysical[];
  socioeconomiclist: SocioEconomic[];
  role: string;
  percentcompleted: number;
  createdby: number;
  updatedon?: string;
}

export interface Search {
  search: string;
}

export interface Meta {
  id: number;
  description: string;
  name: string;
  caption: string;
  validationregex?: any;
  validationmessage?: any;
  datatype: string;
  min?: any;
  max?: any;
  required: boolean;
  displayorder: number;
  listofvalues: (string)[];
  controltype: string;
  biophysicalid: number;
  defaultvisible: boolean;
  defaultvisibleid?: number;
  data: Data;
}

export interface SocioEconomic {
  Name: string;
  SocioEconomicId: number;
  Meta: Meta[];
  Data: SocioVariable[][];
}

export interface BioPhysical {
  Name: string;
  Id: number;
  Rows: string;
  Cols: string;
  Meta: Meta[];
  Data: BioVariable[][];
}

export interface Data {
  id?: number;
  value: string;
  rowindex?: number;
}

export interface BioVariable {
  value: string;
  rowindex?: any;
  variableid?: any;
  planid?: any;
  id: any;
}

export interface BioSetting {
  visible: boolean;
  biophysicalvariableid: number;
  planid: number;
  id?: number;
}

export interface SocioVariable {
  value: string;
  rowindex?: any;
  variableid?: any;
  planid?: any;
  id: any;
  palikaid?: number;
  palikaname?: string;
  file?: any;
}

export interface SocioEconomicSetting {
  visible: boolean;
  socioeconomicvariableid: number;
  planid: number;
  id?: number;
}

export interface SubWatershedDistrictPalika {
  id: number;
  districtid: number;
  subwatershedid: number;
  palikaid: number;
  district: string;
  subwatershed: string;
  palika: string;
}

export interface PlanStakeholderModel {
  id: number;
  stakeholder: string;
  categoryid: number;
  categoryname?: string;
  engagementarea: string;
  contactperson: string;
  contactnumber: number;
  contactaddress: string;
  planid: number;
}

export interface wmpsPlan {
  total_pages: number;
  count: number;
  previous?: string;
  next?: string;
  results: WatershedPlan[];
}

export interface CategoryBasedDisaster{
  scenarioid: number;
  scenarioname: string;
}

export interface ScenarioBasedManagementCondition{
  managementconditionid: number;
  managementconditionname: string;
}

export interface ContingencyPlan{
  html: string;
}

export interface ManagementConditionBasedManagementSubCondition{
  managementsubconditionid: number;
  managementsubconditionname: string;
}

// export interface PoultryContingencyPlan{
//   html: string;
// }