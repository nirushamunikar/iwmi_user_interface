export interface Organisation {
    Id: number;
    OrganisationName?: any;
    OwnershipStatus?: any;
    Province?: any;
    District?: any;
    Palika?: any;
    WardNumber: number;
    needToUpdateAnnualOrganisationUpdate: boolean;
    annualOrganisationForDate?: any;
}

export interface Module {
    Category: string;
    Title: string;
    Path: string;
    ShowCategoryMenu: boolean;
    ShowSubCategoryMenu: boolean;
    CategoryOrder: number;
    SubCategoryOrder: number;
    Icon: string;
    IsDefault: boolean;
    CanApprove: boolean;
}

export interface Category {
    Category: string;
    Icon: string;
}

export interface UserInfo {
    UserId: number;
    FYId: number;
    FYName: string;
    FYTrimester?: any;
    FYStartDate: string;
    FYEndDate: string;
    UserName: string;
    RoleId: number;
    RoleName: string;
    Token: string;
    SpecialPermission: number;
    Organisation: Organisation;
    Modules: Module[];
    Categories: Category[];
}
