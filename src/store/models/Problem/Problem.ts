export interface ProblemModel {
  id: number;
  name: string;
  map?: any;
  location?: string;
  area?: any;
  length?: any;
  cause: string;
  severity: string;
  selected: boolean;
  problemtypeid?: number;
  problemsubtypeid?: number;
  activityid?: number;
  planid: number;
  programid?: number;
  elementaffected: ElementAffectedModel[];
  scopeofproblems: ScopeOfProblem[];
  programname?: string;
  activityname?: string;
  rank?: number;
  avg?: number;
  problemtype?: string;
  problemsubtype?: string;
  problemserialdetail: string;
  potentialobstacles?: string;
  objectives?: string;
  total_plan_year?: string;
}

export interface ElementAffectedModel {
  id: number;
  houses: number;
  schools: number;
  healthpost: number;
  otherbuildings: number;
  roads: number;
  bridges: number;
  irrigationscheme: number;
  agricultureland: number;
  forest: number;
  telecom: number;
  electricity: number;
  hydropower: number;
  others: string;
  problemid: number;
}

export interface ProblemRankModel {
  id: number;
  problemid: number;
  criteriaid: number;
  value: number;
}

export interface ScopeOfProblem {
  id: number;
  problemid: number;
  activityid: number;
  scopeofactivityid: number;
}

export interface ProblemRankVariable {
  index: number;
  avg: number;
  rank: number;
}

export interface ProblemCriteriaWithRank {
  id: number;
  problem: string;
  criteria: { [key: number]: number };
  weightage: { [key: number]: number };
  avg: number;
  rank?: number;
  selected: boolean;
  error?: boolean;
  planid?: number;
  problemweightageid: number;
}
