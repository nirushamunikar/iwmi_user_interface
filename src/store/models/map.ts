export interface GroupedLayer {
  [keys: string]: Layers[];
}

export interface Focus {
  extent: [number, number, number, number];
  swid?: number;
  geodata: string;
}

export interface LayerGroup {
  id: number;
  priority: number;
  name: string;
}

export interface Layers {
  id: number;
  layer_id: string;
  source: number;
  source_data: Source;
  description: string;
  source_description: string;
  priority: number;
  title: string;
  group: string;
  group_id: number;
  cmp: string;
  visible: boolean;
}
export interface Source {
  id: number;
  cmp: string;
  url: string;
  layers: string;
  legend: string;
  serverType: string;
}

export interface Feature {
  name: string;
  properties: {
    caption: string;
    data: {
      variableid: number;
      property: string;
      value: string;
    }[];
  }[];
}

// export interface Feature {
//   type: string;
//   features?: (any)[] | null;
//   totalFeatures: string;
//   numberReturned: number;
//   timeStamp: string;
//   crs?: any | null;
// }
