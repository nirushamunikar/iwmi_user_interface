export interface Page {
  next?: string;
  previous?: string;
  count: number;
  total_pages: number;
  results: any[];
}

export interface Province {
  id: number;
  name: string;
  scode: number;
  selected: boolean;
  isIndeterminate: boolean;
  selectall: boolean;
}

export interface District {
  id: number;
  name: string;
  scode: number;
  dcode: number;
  sdd: number;
  clustercode?: null;
  sdcode?: null;
  cdcode?: null;
  provinceid: number;
  selected: boolean;
}

export interface Palika {
  id: number;
  name: string;
  type: string;
  scode: number;
  dcode: number;
  mcode: number;
  sddmm: number;
  sdcode: string;
  cdcode: string;
  districtid: number;
}

export interface IdName {
  id: number;
  name: string;
}

export interface Activity {
  id: number;
  name: string;
  programid: number;
}

export interface ScopeOfActivity {
  id: number;
  activityid: number;
  name: string;
  unit: string;
  rate: number;
}

export interface Criteria {
  id: number;
  name: string;
  description: string;
  cn: string;
  selected: boolean;
  weightage: number;
  problemweightageid: number;
  ratinginfo: string;
}

export interface ProblemSubTypesModel {
  id: number;
  name: string;
  problemtypeid: number;
  description: string;
  causes: string;
}

export interface ObjectiveGroup {
  activity: string;
  objectives: Objective[];
}

export interface Objective {
  id: number;
  activiyid: number;
  name: string;
  selected: boolean;
}

export interface Role {
  id: number;
  role: string;
}

export interface User {
  id: number;
  user: number;
  username: string;
  firstname: string;
  lastname: string;
  email: string;
  planner_district: string[];
  manager_district: string[];
  password?: string;
  repeatpassword?: string;
  designation: string;
}
