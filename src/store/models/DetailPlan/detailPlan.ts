export interface ActivityStakeholderModel {
  id: number;
  problemid: number;
  activityid: number;
  planstakeholderid: number;
  // categoryid: number;
  operational: boolean;
  possibleimpact: string;
  contributionid: number;
  stakeholdername?: string;
  // categoryname?: string;
  contributionname?: string;
}

export interface ScopeOfActivityDetailModel {
  id: number;
  problemid: number;
  activityid: number;
  scopeofactivityid: number;
  quantity: number;
  // unit: string;
  implementationyear: number;
  cost: number;
  scopeofactivityname?: string;
  error?: boolean;
  rate: number;
}

export interface Meta {
  id: number;
  description: string;
  name: string;
  caption: string;
  validationregex?: any;
  validationmessage?: any;
  datatype: string;
  min?: any;
  max?: any;
  required: boolean;
  displayorder: number;
  listofvalues: (string)[];
  controltype: string;
  defaultvisible: boolean;
  defaultvisibleid?: number;
}

export interface Activity {
  Name: string;
  ActivityId: number;
  Meta: Meta[];
  Data: ActivityVariable[][];
}
export interface ActivityVariable {
  id: any;
  value: string;
  rowindex?: any;
  variableid?: any;
  planid?: any;
  problemid?: any;
  activityid?: any;
  file?: any;
}

export interface ActivitySetting {
  visible: boolean;
  activityinformationvariableid: number;
  planid: number;
  id?: number;
}

export interface ImageModel {
  value?: string;
  rowindex?: number;
  variableid?: number;
  planid?: number;
  id?: number;
  file?: any[];
  name?: string;
  url?: string;
}
