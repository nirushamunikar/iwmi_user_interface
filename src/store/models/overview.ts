import {
  WatershedPlan,
  BioPhysical,
  SocioEconomic,
  PlanStakeholderModel
} from "@/store/models/plan/watershedPlan";
import {
  ProblemModel,
  ProblemCriteriaWithRank
} from "@/store/models/Problem/Problem";
import {
  ActivityStakeholderModel,
  ScopeOfActivityDetailModel,
  Activity
} from "@/store/models/DetailPlan/detailPlan";

export interface Overview {
  plan: WatershedPlan;
  biophysical: BioPhysical;
  socioeconomic: SocioEconomic;
  potentialStakeholder: PlanStakeholderModel;
  problemList: ProblemModel[];
  problemCriteria: ProblemCriteriaWithRank[];
  selectedProblem: {
    Problem: ProblemModel[];
    Stakeholders: ActivityStakeholderModel[];
    ScopeOfActivity: ScopeOfActivityDetailModel[];
    Activity: Activity[];
  }[];
}

export interface ActivityStakeholder {
  id: number;
  problemid: number;
  activityid: number;
  planstakeholderid: number;
  categoryid: number;
  operational: boolean;
  possibleimpact: string;
  contributionid: number;
  stakeholdername?: string;
  categoryname?: string;
  contributionname?: string;
  budgetid?: number;
  budget?: number;
}

export interface WorkPlan {
  activity: string;
  program: string;
  scopeOfActivities: ScopeOfActivityDetailModel;
  stakeholders: ActivityStakeholder[];
}

export interface Evaluation {
  id: number;
  completionstatus: string;
  progress: number;
  remarks: string;
  year: number;
  scopeofactivitydetail: ScopeOfActivityDetailModel;
}

export interface Profile {
  id: number;
  planid: number;
  activityid: number;
  problemid: number;
  budget: number;
  actualexpendituretotal: number;
  actualexpendituredsco: number;
  actualexpenditureuser: number;
  benefitedhouseholdtotal: number;
  benefitedhouseholddalit: number;
  benefitedhouseholdjanjati: number;
  benefitedhouseholdothers: number;
  employmentgeneratedtotal: number;
  employmentgenerateddalit: number;
  employmentgeneratedjanjati: number;
  employmentgeneratedothers: number;
  problemfaced: string;
  maintenance: string;
  lesson: string;
  otherdetail: string;
  fiscalyear: string;
  dscodistrict: string;
  estimatedcost: number;
  usercost: number;
  objectives: string[];
  scopeofactivity: string[];
  beforepictures: any[];
  afterpictures: any[];
  document?: string;
  document_name?: string;
}
