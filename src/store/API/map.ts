import { Action } from "@/store/actionType";
import { getApi, postApi, deleteApi, getApiWithoutBack } from "@/store/API/api";

export default class Map {
  public static async LayerList(params: any = "/"): Promise<any> {
    const response = await getApiWithoutBack(Action.LayerList + params);
    return response as any;
  }
  public static async LayerGroupList(params: any = "/"): Promise<any> {
    const response = await getApiWithoutBack(Action.LayerGroup + params);
    return response as any;
  }
  public static async LayerSourceList(params: any = "/"): Promise<any> {
    const response = await getApiWithoutBack(Action.LayerSource + params);
    return response as any;
  }
  public static async Extent(params: any): Promise<any> {
    const response = await getApiWithoutBack(Action.Extent + "/" + params);
    return response as any;
  }
  public static async FeatureList(params: any): Promise<any> {
    const response = await postApi(Action.GeoServerProxy, params);
    return response as any;
  }
  public static async MapData(params: any): Promise<any> {
    const response = await postApi(Action.MapData, params);
    return response as any;
  }

  public static async saveLayers(layers: any) {
    const response = await postApi(Action.LayerList, layers);
    return response as any;
  }

  public static async saveLayerGroup(layers: any) {
    const response = await postApi(Action.LayerGroup, layers);
    return response as any;
  }

  public static async uploadMapPicture(data: any) {
    const response = await postApi(Action.GenerateDocument, data, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
    return response as any;
  }

  public static async saveLayerSource(layerSource: any) {
    const response = await postApi(Action.LayerSource, layerSource);
    return response as any;
  }

  public static async deleteLayer(layerId: string): Promise<any> {
    const response = await deleteApi(Action.LayerList, layerId);
    return response as any;
  }

  public static async deleteLayerGroup(layerGroupId: number): Promise<any> {
    const response = await deleteApi(Action.LayerGroup, layerGroupId);
    return response as any;
  }

  public static async deleteLayerSource(layerSourceId: number): Promise<any> {
    const response = await deleteApi(Action.LayerSource, layerSourceId);
    return response as any;
  }
}
