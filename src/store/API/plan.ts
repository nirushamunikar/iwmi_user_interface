import { UserSubmit } from "@/store/models/Variable";
import { Action } from "@/store/actionType";
import {
  WatershedPlan,
  BioVariable,
  BioSetting,
  SocioVariable,
  SocioEconomic,
  SocioEconomicSetting,
  PlanStakeholderModel,
  Search
} from "@/store/models/plan/watershedPlan";
import {
  postApi,
  getApi,
  getApiWithoutBack,
  deleteApi,
  deleteApiWithoutBack
} from "@/store/API/api";

export default new (class PlanAPI {
  id: string = "val";

  public async loginUser(user: UserSubmit): Promise<any> {
    const response = await postApi(Action.Login, {
      UserName: user.UserName,
      Password: user.Password,
      DeviceType: user.DeviceType
    });
    return response as any;
  }

  public async getSubwatershedById(id: number): Promise<any> {
    const url = Action.SubWatershedDistrictPalika;
    const response = await postApi(url, { subwatershedid: id });
    return response as any;
  }

  public async savePlan(plan: WatershedPlan): Promise<any> {
    const response = await postApi(Action.Plan, plan);
    return response as any;
  }

  // public static async  postNewPlan(planId: number): Promise<any> {
  //     const action = Action.Plan + "/" + planId
  //     const response = await postApi(action, planId);
  //     return response as any;
  // }

  public async getPlan(
    params: {
      page?: any;
      search?: any;
      other?: boolean;
    } = {
        other: false
      }
  ): Promise<any> {
    let query = "/";
    if (params.search || params.page || params.other) {
      query += "?";
    }
    if (params.search) {
      query += "&search=" + params.search;
    }
    if (params.page) {
      query += "&page=" + params.page;
    }
    if (params.other) {
      query += "&other=" + params.other;
    }
    const response = await getApiWithoutBack(Action.Plan + query);
    return response as any;
  }

  public async getPlanPage(page: number): Promise<any> {
    const response = await getApiWithoutBack(Action.Plan + "/?page=" + page);
    return response as any;
  }

  public async getPlanPageOther(page: number): Promise<any> {
    const response = await getApiWithoutBack(
      Action.Plan + "/?page=" + page + "&other=true"
    );
    return response as any;
  }

  public async getPlanById(planId: number): Promise<any> {
    const response = await getApi(Action.Plan + "/" + planId);
    return response as any;
  }

  public async getPlanBySearch(search: string): Promise<any> {
    const response = await getApiWithoutBack(
      Action.Plan + "/?search=" + search
    );
    return response as any;
  }

  public async getSocioEconomic(planId: number): Promise<any> {
    const action = Action.SocioEconomicByPlan + "/" + planId;
    const response = await getApi(action);
    return response as any;
  }

  public async getSocioEconomicFeatures(planId: number): Promise<any> {
    const action = Action.SocioEconomicFeaturesByPlan + "/" + planId;
    const response = await getApi(action);
    return response as any;
  }

  public async getBioPhysical(planId: number): Promise<any> {
    const action = Action.BioPhysicalByPlan + "/" + planId;
    const response = await getApi(action);
    return response as any;
  }

  public async saveBioPhysical(ls: BioVariable[]): Promise<any> {
    const action = Action.BioPhysicalByPlan;
    const response = await postApi(action, ls);
    // console.log(response);
    return response as any;
  }

  public async saveBioPhysicalSetting(ls: BioSetting[]): Promise<any> {
    const action = Action.BioPhysicalSettings;
    const response = await postApi(action, ls);
    // console.log(response);
    return response as any;
  }

  public async saveSocioEconomic(ls: SocioVariable[][][]): Promise<any> {
    const action = Action.SocioEconomicByPlan;
    const response = await postApi(
      action,
      ls
      //   , {
      //   headers: {
      //     "Content-Type": "multipart/form-data"
      //   }
      // }
    );
    // console.log(response);
    return response as any;
  }

  public async saveSocioEconomicSetting(
    ls: SocioEconomicSetting[]
  ): Promise<any> {
    const action = Action.SocioEconomicSettings;
    const response = await postApi(action, ls);
    // console.log(response);
    return response as any;
  }

  public async savePlanStakeholder(
    planstakeholder: PlanStakeholderModel[]
  ): Promise<any> {
    const response = await postApi(Action.PlanStakeholder, planstakeholder);
    return response as any;
  }

  public async updatePlanPercentage(param: {
    planid: number;
    percentcompleted: number;
  }): Promise<any> {
    const response = await postApi(Action.PlanCompletionPercentage, param);
    return response as any;
  }

  public async getStakeholderListByPlanId(planId: number): Promise<any> {
    const url = Action.PlanStakeholder + "/?planid=" + planId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }

  public async deleteStakeholder(stakeholderId: number): Promise<any> {
    const response = await deleteApi(Action.PlanStakeholder, stakeholderId);
    return response as any;
  }

  public async deleteSocioEconomic(
    planId: number,
    socioeconomicId: number,
    rowindex: number
  ): Promise<any> {
    let param =
      planId + "/?socioeconomicid=" + socioeconomicId + "&rowindex=" + rowindex;
    const response = await deleteApiWithoutBack(
      Action.SocioEconomicByPlan,
      param
    );
    return response as any;
  }

  public async getScenario(district : any, crop: any){
    let params ="/?districtid=" + district + "&category=" + crop;
    const response = await getApiWithoutBack(Action.CategoryBasedScenarios + params);
    return response as any;
  }

  public async getSituation(district : any, crop: any, scenario:any){
    let params ="/?districtid=" + district + "&category=" + crop + "&scenarioid=" + scenario;
    const response = await getApiWithoutBack(Action.ScenarioBasedManagementCondition + params);
    return response as any;
  }

  public async getSubCondition(district : any, crop: any, scenario:any, condition:any){
      let params ="/?districtid=" + district + "&category=" + crop + "&scenarioid=" + scenario +
      "&managementconditionid=" + condition;
      const response = await getApiWithoutBack(Action.ManagementConditionBasedManagementSubCondition + params);
      return response as any;
    }

  public async gethtml(district : any, crop: any, scenario:any, condition:any){
    let params ="/?districtid=" + district + "&category=" + crop + "&scenarioid=" + scenario + "&managementconditionid=" + condition;
    const response = await getApiWithoutBack(Action.ContingencyPlan + params);
    return response as any;
  }

  public async gethtmlCropFisheries(district : any, crop: any, scenario:any, condition:any, subcondition:any){
    let params ="/?districtid=" + district + "&category=" + crop + "&scenarioid=" + scenario +
        "&managementconditionid=" + condition + "&managementsubconditionid=" + subcondition;
    const response = await getApiWithoutBack(Action.ContingencyPlan + params);
    return response as any;
  }

  // public async gethtmlPoultry(district : any, crop: any, scenario:any, condition:any){
  //   let params ="/?districtid=" + district + "&category=" + crop + "&scenarioid=" + scenario + "&condition=" + condition;
  //   const response = await getApiWithoutBack(Action.PoultryContingencyPlan + params);
  //   return response as any;
  // }

})();
