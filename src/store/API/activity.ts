import { getApi } from "@/store/API/api";
import { Action } from "@/store/actionType";

export default new class ProblemAPI {
  public async getActivityByProblemId(id: number): Promise<any> {
    const url = Action.ActivityByProblem + "/" + id;
    const response = await getApi(url);
    return response as any;
  }
}();
