import { postApi, getApi, deleteApi } from "@/store/API/api";
import { Action } from "@/store/actionType";
import {
  ProblemModel,
  ElementAffectedModel,
  ProblemCriteriaWithRank
} from "@/store/models/Problem/Problem";

export default new (class ProblemAPI {
  public async getProblemsByPlanId(id: number): Promise<any> {
    const url = Action.Problem + "/" + id;
    const response = await getApi(url);
    return response as any;
  }
  public async getSelectedProblemsByPlanId(id: number): Promise<any> {
    const url = Action.SelectedProblem + "/" + id;
    const response = await getApi(url);
    return response as any;
  }
  public async getElementAffectedListByProblemId(id: number): Promise<any> {
    const url = Action.ProblemElementAffected + "/" + id;
    const response = await getApi(url);
    return response as any;
  }
  public async getScopeOfProblemByProblemId(id: number): Promise<any> {
    const url = Action.ProblemScopeOfProblem + "/" + id;
    const response = await getApi(url);
    return response as any;
  }

  public async saveProblem(problem: ProblemModel): Promise<any> {
    const response = await postApi(Action.Problem, problem);
    return response as any;
  }
  public async saveElementAffected(
    problem: ElementAffectedModel
  ): Promise<any> {
    const response = await postApi(Action.ProblemElementAffected, problem);
    return response as any;
  }

  public async getProblemCriteriaListByPlanId(id: number): Promise<any> {
    const url = Action.ProblemCriteria + "/" + id;
    const response = await getApi(url);
    return response as any;
  }

  public async saveProblemCriteria(
    problemcriterias: ProblemCriteriaWithRank[]
  ): Promise<any> {
    const response = await postApi(Action.ProblemCriteria, problemcriterias);
    return response as any;
  }

  public async getActivityListByProblemId(id: number): Promise<any> {
    const url = Action.DetailActivity + "/" + id;
    const response = await getApi(url);
    return response as any;
  }

  public async saveProblemWeightage(saveData: any[]): Promise<any> {
    const url = Action.Weightage;
    const response = await postApi(url, saveData);
    return response as any;
  }

  public async deleteProblem(problemId: number): Promise<any> {
    const response = await deleteApi(Action.Problem, problemId);
    return response as any;
  }
})();
