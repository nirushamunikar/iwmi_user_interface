import axios from "axios";
import Vue from "vue";
import { Action } from "@/store/actionType";
import { eventHub } from "@/utils/eventHub";
import JWTStore from "@/store/modules/jwt";

const actionBase = axios.create({
  baseURL: Action.Base
});

actionBase.defaults.headers.common["Authorization"] =
  "Bearer " + localStorage.getItem("access_token");

actionBase.interceptors.response.use(
  function(response) {
    return response;
  },
  async function(error) {
    if (error.response.status === 401) {
      if (
        error.response.data.code === "token_not_valid" &&
        (error.response.data.detail === "Token is invalid or expired" ||
          error.response.data.detail === "Token 'exp' claim has expired")
      ) {
        JWTStore.logOut();
      } else if (
        error.response.data.code === "token_not_valid" &&
        error.response.data.messages[0]["token_class"] === "AccessToken"
      ) {
        const tok = await JWTStore.getAccessToken();
        return new Promise(resolve => {
          error.response.config.headers.Authorization =
            "Bearer " + localStorage.getItem("access_token");
          resolve(axios(error.response.config));
        });
      }
    }

    return Promise.reject(error);
  }
);

export async function postApi(
  action: string,
  param: any,
  extra?: any
): Promise<any> {
  eventHub.$emit("before-request");
  return await new Promise((resolve, reject) => {
    setJWT();
    if (
      action === "forgotpassword" ||
      action === "resetpassword" ||
      action === "changesuccess"
    ) {
      clearJWT();
    } else {
    }

    // setJWT();

    let promise = actionBase.post("/" + action + "/", param, extra);
    promise.then(
      res => {
        eventHub.$emit("after-response");
        resolve(res.data);
      },
      err => {
        eventHub.$emit("after-response");
        var vm = new Vue();
        if (err.response) {
          if (err.response.status === 400 && err.response.data.new_username) {
            for (var i = 0; i < err.response.data.new_username.length; i++) {
              vm.$snotify.error(err.response.data.new_username[i], "error");
            }
          }
          // else if (
          //   err.response.status === 400 &&
          //   err.response.data.refresh[0] === "This field may not be null."
          // ) {
          //   JWTStore.logOut();
          // }
          else if (
            err.response.status === 400 &&
            err.response.data.code === "token_not_valid" &&
            err.response.data.detail === "Invalid Token"
          ) {
            // vm.$snotify.error("Verifying Token", "Error");
          }
        }
      }
    );
    promise.catch(err => {
      eventHub.$emit("after-response");
      var vm = new Vue();
      if (err.response) {
        if (err.response.status === 400 && err.response.data.non_field_errors) {
          for (var i = 0; i < err.response.data.non_field_errors.length; i++) {
            vm.$snotify.error(err.response.data.non_field_errors[i], "error");
          }
        } else if (err.response.status === 400 && err.response.data.username) {
          for (var i = 0; i < err.response.data.username.length; i++) {
            vm.$snotify.error(err.response.data.username[i], "error");
          }
        } else if (err.response.status === 401) {
          vm.$snotify.error(err.response.data.detail, "Error");
        }
      }
    });
    promise.catch(err => {
      eventHub.$emit("after-response");
      var vm = new Vue();
      if (err.response) {
        if (err.response.status === 400 && err.response.data.new_password) {
          for (var i = 0; i < err.response.data.new_password.length; i++) {
            vm.$snotify.error(err.response.data.new_password[i], "error");
          }
        }
        if (err.response.status === 400 && err.response.data.current_password) {
          for (var i = 0; i < err.response.data.current_password.length; i++) {
            vm.$snotify.error(err.response.data.current_password[i], "error");
          }
        }
      } else {
        eventHub.$emit("network-error");
      }
    });
    // promise.catch(err => {
    //   eventHub.$emit("after-response");
    //   eventHub.$emit("network-error");
    // });
  });
}

export async function getApiWithoutBack(action: string): Promise<any> {
  return await new Promise((resolve, reject) => {
    setJWT();
    eventHub.$emit("before-request");
    actionBase.get("/" + action).then(
      res => {
        eventHub.$emit("after-response");
        resolve(res.data);
      },
      err => {
        eventHub.$emit("after-response");

        var vm = new Vue();
        if (err.response) {
          vm.$snotify.error(err.response.data.Message, "error");
        } else {
          eventHub.$emit("network-error");
        }
      }
    );
  });
}
export async function getApi(action: string): Promise<any> {
  return await new Promise((resolve, reject) => {
    setJWT();
    eventHub.$emit("before-request");
    actionBase.get("/" + action + "/").then(
      res => {
        eventHub.$emit("after-response");
        resolve(res.data);
      },
      err => {
        eventHub.$emit("after-response");
        var vm = new Vue();
        if (err.response) {
          vm.$snotify.error(err.response.data.Message, "error");
        } else {
          eventHub.$emit("network-error");
        }
      }
    );
  });
}

export async function deleteApi(action: string, param: any): Promise<any> {
  eventHub.$emit("before-request");
  return await new Promise((resolve, reject) => {
    setJWT();
    actionBase.delete("/" + action + "/" + param + "/").then(
      res => {
        eventHub.$emit("after-response");
        resolve(res.data);
      },
      err => {
        eventHub.$emit("after-response");
        var vm = new Vue();
        if (err.response) {
          if (err.response.data.Message) {
            vm.$snotify.error(err.response.data.Message, "error");
          }
          if (err.response.data.message) {
            vm.$snotify.error(err.response.data.message, "error");
          }
        } else {
          eventHub.$emit("network-error");
        }
      }
    );
  });
}

export async function deleteApiWithoutBack(
  action: string,
  param: any
): Promise<any> {
  eventHub.$emit("before-request");
  return await new Promise((resolve, reject) => {
    setJWT();
    actionBase.delete("/" + action + "/" + param).then(
      res => {
        eventHub.$emit("after-response");
        resolve(res.data);
      },
      err => {
        eventHub.$emit("after-response");
        var vm = new Vue();
        if (err.response) {
          vm.$snotify.error(err.response.data.Message, "error");
        } else {
          eventHub.$emit("network-error");
        }
      }
    );
  });
}

// Make Axios play nice with Django CSRF
axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.xsrfHeaderName = "X-CSRFToken";

export function setJWT() {
  if (localStorage.getItem("access_token")) {
    actionBase.defaults.headers.common["Authorization"] =
      "Bearer " + localStorage.getItem("access_token");
  } else {
    // console.log("access token: " + localStorage.getItem("access_token"));
  }
}

export function clearJWT() {
  delete actionBase.defaults.headers.common["Authorization"];
}
