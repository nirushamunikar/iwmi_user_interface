import {
  postApi,
  getApi,
  getApiWithoutBack,
  deleteApi,
  deleteApiWithoutBack
} from "@/store/API/api";
import { Action } from "@/store/actionType";
import { ImageModel } from "@/store/models/DetailPlan/detailPlan";

export default new (class ImageAPI {
  public async saveImage(image: ImageModel): Promise<any> {
    const response = await postApi(Action.Test, image, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
    return response as any;
  }
  public async deleteImage(id: number): Promise<any> {
    const response = await deleteApi(Action.Test, id);
    return response as any;
  }
})();
