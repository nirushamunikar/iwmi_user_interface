import {
  postApi,
  getApi,
  getApiWithoutBack,
  deleteApi,
  deleteApiWithoutBack
} from "@/store/API/api";
import { Action } from "@/store/actionType";
import {
  ActivityStakeholderModel,
  ScopeOfActivityDetailModel,
  ActivityVariable,
  ActivitySetting
} from "@/store/models/DetailPlan/detailPlan";

export default new (class DetailPlanAPI {
  public async geActivityDetailInformation(
    planId: number,
    problemId: number,
    activityId: number
  ): Promise<any> {
    const url =
      Action.ActivityDetailInformation +
      "/?planid=" +
      planId +
      "&problemid=" +
      problemId +
      "&activityid=" +
      activityId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }

  public async getActivityDetailFromODK(problemId: number): Promise<any> {
    const url =
      Action.ActivityDetailInformationODK + "/?problemid=" + problemId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }

  public async saveAdditionalInformation(
    ls: ActivityVariable[][][]
  ): Promise<any> {
    const action = Action.ActivityDetailInformation;
    const response = await postApi(action, ls);
    // console.log(response);
    return response as any;
  }

  public async saveActivityInformationSetting(
    ls: ActivitySetting[]
  ): Promise<any> {
    const action = Action.ActivitySetting;
    const response = await postApi(action, ls);
    // console.log(response);
    return response as any;
  }

  public async saveActivityStakeholder(
    activitystakeholder: ActivityStakeholderModel[]
  ): Promise<any> {
    const response = await postApi(
      Action.ActivityStakeholder,
      activitystakeholder
    );
    return response as any;
  }

  public async getStakeholderListByProblemId(id: number): Promise<any> {
    const url = Action.ProblemStakeholder + "/" + id;
    const response = await getApi(url);
    return response as any;
  }

  public async getPossibleContribution(): Promise<any> {
    const response = await getApi(Action.PossibleContribution);
    return response as any;
  }

  public async getStakeholderCategory(): Promise<any> {
    const response = await getApi(Action.StakeholderCategory);
    return response as any;
  }

  public async getActivityStakeholderListbyActivityId(obj: any): Promise<any> {
    const url =
      Action.ActivityStakeholder +
      "/?problemid=" +
      obj.problemId +
      "&activityid=" +
      obj.activityId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }

  public async getScopeOfActivityDetailListbyActivityId(
    obj: any
  ): Promise<any> {
    const url =
      Action.ScopeOfActivityDetail +
      "/?problemid=" +
      obj.problemId +
      "&activityid=" +
      obj.activityId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }

  public async saveScopeOfActivityDetail(
    scopeofactivitydetail: ScopeOfActivityDetailModel
  ): Promise<any> {
    const response = await postApi(
      Action.ScopeOfActivityDetail,
      scopeofactivitydetail
    );
    return response as any;
  }

  public async deleteStakeholderDetail(stakeholderId: number): Promise<any> {
    const response = await deleteApi(Action.ActivityStakeholder, stakeholderId);
    return response as any;
  }

  public async deleteScopeOfActivityDetail(soaId: number): Promise<any> {
    const response = await deleteApi(Action.ScopeOfActivityDetail, soaId);
    return response as any;
  }

  public async deleteAdditionalInformation(
    planId: number,
    problemId: number,
    activityId: number,
    activitygroupId: number,
    rowindex: number
  ): Promise<any> {
    let param =
      planId +
      "/?problemid=" +
      problemId +
      "&activityid=" +
      activityId +
      "&activitygroupid=" +
      activitygroupId +
      "&rowindex=" +
      rowindex;
    const response = await deleteApiWithoutBack(
      Action.ActivityDetailInformation,
      param
    );
    return response as any;
  }
})();
