import { Action } from "@/store/actionType";
import {
  getApi,
  postApi,
  getApiWithoutBack,
  deleteApi,
  deleteApiWithoutBack
} from "@/store/API/api";

export default class Meta {
  public static async Provincelisting(): Promise<any> {
    const response = await getApi(Action.ProvinceList);
    return response as any;
  }
  public static async Districtlisting(): Promise<any> {
    const response = await getApi(Action.DistrictList);
    return response as any;
  }
  public static async WatershedList(): Promise<any> {
    const response = await getApi(Action.WatershedList);
    return response as any;
  }
  public static async SubWatershedList(): Promise<any> {
    const response = await getApi(Action.SubWatershedList);
    return response as any;
  }
  public static async PalikaList(): Promise<any> {
    const response = await getApi(Action.PalikaList);
    return response as any;
  }
  public static async ProgramTypes(): Promise<any> {
    const response = await getApi(Action.ProblemType);
    return response as any;
  }

  public static async ProblemSubTypes(): Promise<any> {
    const response = await getApi(Action.ProblemSubType);
    return response as any;
  }

  public static async Programs(params: string = "/"): Promise<any> {
    let response = await getApiWithoutBack(Action.Program + params);
    return response as any;
  }

  public static async savePrograms(params: any) {
    const response = await postApi(Action.Program, params);
    return response as any;
  }

  public static async deleteProgram(programId: number): Promise<any> {
    const response = await deleteApi(Action.Program, programId);
    return response as any;
  }

  public static async Activities(params: string = "/"): Promise<any> {
    let response = await getApiWithoutBack(Action.Activity + params);
    return response as any;
  }

  public static async saveActivities(params: any) {
    const response = await postApi(Action.Activity, params);
    return response as any;
  }

  public static async deleteActivity(activityId: number): Promise<any> {
    const response = await deleteApi(Action.Activity, activityId);
    return response as any;
  }

  public static async ScopeOfActivities(
    params: string = "/?no_paging=true"
  ): Promise<any> {
    let response = await getApiWithoutBack(Action.ScopeOfActivity + params);
    return response as any;
  }

  public static async saveScopeOfActivities(params: any) {
    const response = await postApi(Action.ScopeOfActivity, params);
    return response as any;
  }

  public static async deleteScopeOfActivity(soaId: number): Promise<any> {
    const response = await deleteApi(Action.ScopeOfActivity, soaId);
    return response as any;
  }

  public static async Criterias(planid: number): Promise<any> {
    const url = Action.Criteria + "/" + planid;
    const response = await getApi(url);
    return response as any;
  }

  public static async StakeholderList(): Promise<any> {
    const response = await getApi(Action.StakeholderList);
    return response as any;
  }

  public static async Objectives(
    params: {
      list: boolean;
      query?: any;
    } = {
      list: false,
      query: "/"
    }
  ): Promise<any> {
    let response: any;
    if (params.list) {
      if (params.query) {
        response = await getApiWithoutBack(
          Action.Objectives + "/?showAsList=true&" + params.query
        );
      } else {
        response = await getApiWithoutBack(
          Action.Objectives + "/?showAsList=true"
        );
      }
    } else {
      if (params.query) {
        response = await getApiWithoutBack(
          Action.Objectives + "/?" + params.query
        );
      } else {
        response = await getApiWithoutBack(Action.Objectives + "/");
      }
    }
    return response as any;
  }

  public static async saveObjectives(params: any) {
    const response = await postApi(Action.Objectives, params);
    return response as any;
  }

  public static async deleteObjective(objectiveId: number): Promise<any> {
    const response = await deleteApi(Action.Objectives, objectiveId);
    return response as any;
  }

  public static async loadUsers(params: any = "/"): Promise<any> {
    const response = await getApiWithoutBack(Action.CustomUser + params);
    return response as any;
  }

  public static async saveUsers(params: any) {
    const response = await postApi(Action.CustomUser, params);
    return response as any;
  }

  public static async loadRoles(): Promise<any> {
    const response = await getApi(Action.Role);
    return response as any;
  }

  public static async getUser() {
    const response = await getApi(Action.User);
    return response as any;
  }

  public static async deleteCustomUser(id: number) {
    const response = await deleteApi(Action.CustomUser, id);
    return response as any;
  }
}
