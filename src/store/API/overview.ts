import { postApi, getApi, getApiWithoutBack, deleteApi } from "@/store/API/api";
import { Action } from "@/store/actionType";

export default new class SituationOverview {
  public async getSituationOverview(planId: number): Promise<any> {
    const url = Action.Overview + "/?planid=" + planId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }
  public async getWorkPlan(planId: number): Promise<any> {
    const url = Action.WorkPlan + "/?planid=" + planId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }
  public async getEvaluation(planId: number): Promise<any> {
    const url = Action.Evaluation + "/?planid=" + planId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }
  public async getActivityProfile(
    planId: number,
    problemId: number,
    activityId: number
  ): Promise<any> {
    const url =
      Action.ActivityProfile +
      "/?planid=" +
      planId +
      "&problemid=" +
      problemId +
      "&activityid=" +
      activityId;
    const response = await getApiWithoutBack(url);
    return response as any;
  }
  public async saveWorkPlan(saveData: any[]): Promise<any> {
    const url = Action.WorkPlan;
    const response = await postApi(url, saveData);
    return response as any;
  }
  public async genDoc(planid: number, forced: boolean): Promise<any> {
    let url = Action.GenerateDocument + "/?planid=" + planid;
    if (forced) {
      url += "&forced=true";
    }
    const response = await getApiWithoutBack(url);
    return response as any;
  }
  public async saveEvaluation(saveData: any[]): Promise<any> {
    const url = Action.Evaluation;
    const response = await postApi(url, saveData);
    return response as any;
  }
  public async saveActivityProfile(saveData: any): Promise<any> {
    const url = Action.ActivityProfile;
    const response = await postApi(url, saveData);
    return response as any;
  }
  public async saveActivityProfilePicture(saveData: any): Promise<any> {
    const url = Action.ActivityProfilePicture;
    const response = await postApi(url, saveData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
    return response as any;
  }

  public async deleteActivityProfilePicture(id: number): Promise<any> {
    const response = await deleteApi(Action.ActivityProfilePicture, id);
    return response as any;
  }
}();
