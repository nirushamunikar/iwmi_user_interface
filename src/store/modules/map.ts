import store from "@/store";
import {
  Module,
  VuexModule,
  MutationAction,
  getModule,
  Mutation,
  Action
} from "vuex-module-decorators";
import API from "@/store/API/map";
import {
  GroupedLayer,
  Layers,
  Feature,
  Focus,
  Source,
  LayerGroup
} from "@/store/models/map";
import { Page } from "@/store/models/meta";

import problemStore from "@/store/modules/problem";
import { ProblemModel } from "@/store/models/Problem/Problem";
import Map from "ol/Map";

import OSM from "ol/source/OSM";
import BingMaps from "ol/source/BingMaps";

import Tile from "ol/layer/Tile";
import TileWMS from "ol/source/TileWMS";

import Polygon from "ol/geom/Polygon";
import LineString from "ol/geom/LineString";
import Overlay from "ol/Overlay";

import VectorSource from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";

import Draw from "ol/interaction/Draw";
import Snap from "ol/interaction/Snap";
import CircleStyle from "ol/style/Circle";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import Style from "ol/style/Style";
import WKT from "ol/format/WKT";
import GeoJSON from "ol/format/GeoJSON";

import axios from "axios";
import GeometryType from "ol/geom/GeometryType";
import OverlayPositioning from "ol/OverlayPositioning";

@Module({ name: "map", dynamic: true, store })
class MapModule extends VuexModule {
  public isLoadingMap = false;
  public isLoadingMapData = false;
  public isMapLoaded = false;
  public LayerList: Layers[] = [];
  public LayerGroupList: LayerGroup[] = [];
  public LayerSourceList: Source[] = [];
  public GroupedLayer: GroupedLayer | null = null;
  public Features: Feature[] = [];
  public map: Map = new Map({});
  public drawDialogVisible: boolean = false;
  public drawType: string | null = null;
  public tileLayerList: Tile[] = [];
  public vectorLayerList: VectorLayer[] = [];
  public groupPage: Page = {} as Page;
  public sourcePage: Page = {} as Page;
  public layerPage: Page = {} as Page;
  public fullScreenMap: boolean = false;

  public currentSWBoundary: any = "";

  @Mutation
  setCurrentSWBoundary(bnd: any) {
    this.currentSWBoundary = bnd;
  }

  @Mutation
  setLoadingMap(val: boolean) {
    this.isLoadingMap = val;
  }
  @Mutation
  setLayers(layers: Layers[]) {
    this.LayerList = layers;
  }
  @Mutation
  setDrawDialogVisible(val: boolean) {
    this.drawDialogVisible = val;
  }
  @Mutation
  setDrawType(val: string | null) {
    this.drawType = val;
  }
  @Mutation
  setMapLoaded() {
    this.isMapLoaded = true;
  }

  @Mutation
  updateLayer(param: { layer: Layers; index: number }) {
    this.LayerList.splice(param.index, 1, param.layer);
  }

  @Mutation
  pushLayer(layer: Layers) {
    this.LayerList.push(layer);
  }

  @Mutation
  toggleMapSize() {
    this.fullScreenMap = !this.fullScreenMap;
  }

  @Mutation
  setFullscreenMapSize(val: boolean) {
    this.fullScreenMap = val;
  }

  @Action
  async deleteLayer(layerId: string) {
    const obj = await API.deleteLayer(layerId);
  }

  @MutationAction
  async saveLayers(layers: Layers[]) {
    const list = await API.saveLayers(layers);
    return { LayerList: list as Layers[] };
  }

  @Action({ commit: "setLayers", rawError: true })
  async loadLayers() {
    this.setLoadingMap(true);
    var list: Layers[] = [] as Layers[];
    if (this.LayerList.length === 0) {
      list = (await API.LayerList()) as Layers[];
    } else {
      list = this.LayerList;
    }
    this.setLoadingMap(false);
    return list as Layers[];
  }

  @MutationAction
  async searchLayers(params: {
    layers: Layers[];
    searchString?: any;
    page?: any;
  }) {
    API.saveLayers(params.layers);
    let query = "/";
    if (params.searchString || params.page) {
      query += "?";
    }
    if (params.searchString) {
      query += "&search=" + params.searchString;
    }
    if (params.page) {
      query += "&paging=true&page=" + params.page;
    }
    const res = await API.LayerList(query);
    const layers = res.results;
    return { LayerList: layers as Layers[], layerPage: res };
  }

  @MutationAction
  async updateLayers(layers: Layers[]) {
    return { LayerList: layers as Layers[] };
  }

  @MutationAction
  async loadGroupedLayers() {
    const layerList = (await API.LayerList()) as [];
    const result: any = {};
    const key = "group";

    // Group listOfLayerGroupedLayer
    layerList.forEach(item => {
      if (!result[item[key]]) {
        result[item[key]] = [];
      }
      result[item[key]].push(item);
    });

    return { GroupedLayer: result as GroupedLayer };
  }

  @MutationAction
  async updateGroupedLayers(groupedLayers: GroupedLayer) {
    return { GroupedLayer: groupedLayers as GroupedLayer };
  }

  @MutationAction
  async saveLayerGroups(layerGroup: LayerGroup[]) {
    const list = await API.saveLayerGroup(layerGroup);
    return { LayerGroupList: list as LayerGroup[] };
  }

  @MutationAction
  async loadLayerSource() {
    let source = await API.LayerSourceList();
    return { LayerSourceList: source as Source[] };
  }

  @MutationAction
  async searchLayerSource(params: {
    source: Source[];
    searchString?: any;
    page?: any;
  }) {
    API.saveLayerSource(params.source);
    let query = "/";
    if (params.searchString || params.page) {
      query += "?";
    }
    if (params.searchString) {
      query += "&search=" + params.searchString;
    }
    if (params.page) {
      query += "&paging=true&page=" + params.page;
    }
    const res = await API.LayerSourceList(query);
    const source = res.results;
    return { LayerSourceList: source as Source[], sourcePage: res };
  }

  @Mutation
  updateLayerSource(param: { layerSource: Source; index: number }) {
    this.LayerSourceList.splice(param.index, 1, param.layerSource);
  }

  @Mutation
  pushLayerSource(layerSource: Source) {
    this.LayerSourceList.push(layerSource);
  }

  @Action
  async deleteLayerSource(layerSourceId: number) {
    const obj = await API.deleteLayerSource(layerSourceId);
  }

  @MutationAction
  async saveLayerSource(layerSource: Source[]) {
    const list = await API.saveLayerSource(layerSource);
    return { LayerSourceList: list as Source[] };
  }

  @MutationAction
  async loadLayerGroup() {
    let source = await API.LayerGroupList();
    return { LayerGroupList: source as LayerGroup[] };
  }

  @MutationAction
  async searchLayerGroup(params: {
    group: LayerGroup[];
    searchString?: any;
    page?: any;
  }) {
    API.saveLayerGroup(params.group);
    let query = "/";
    if (params.searchString || params.page) {
      query += "?";
    }
    if (params.searchString) {
      query += "&search=" + params.searchString;
    }
    if (params.page) {
      query += "&paging=true&page=" + params.page;
    }
    const page = await API.LayerGroupList(query);
    const group = page.results;
    return { LayerGroupList: group as LayerGroup[], groupPage: page };
  }

  @Mutation
  updateLayerGroup(param: { layerGroup: LayerGroup; index: number }) {
    this.LayerGroupList.splice(param.index, 1, param.layerGroup);
  }

  @Mutation
  pushLayerGroup(layerGroup: LayerGroup) {
    this.LayerGroupList.push(layerGroup);
  }

  @Action
  async deleteLayerGroup(layerGroupId: number) {
    if (layerGroupId !== 0) {
      const obj = await API.deleteLayerGroup(layerGroupId);
    }
  }

  @MutationAction
  async updateFeatures(url: string[]): Promise<any> {
    const features = await API.FeatureList({ url: url });
    return { Features: features as Feature[] };
  }

  @MutationAction
  async loadMapData(subwatershedid: any): Promise<any> {
    this.isLoadingMapData = true;
    const features = await API.MapData(subwatershedid);
    this.isLoadingMapData = false;
    return { Features: features as Feature[] };
  }

  /**
   * Map Related functions
   */

  get getMap() {
    return this.map;
  }

  strokeStyle = new Style({
    fill: new Fill({
      color: "rgba(255, 255, 255, 0.2)"
    }),
    stroke: new Stroke({
      color: "#ffcc33",
      width: 2
    }),
    image: new CircleStyle({
      radius: 7,
      fill: new Fill({
        color: "#ffcc33"
      })
    })
  });

  @MutationAction
  async setTileLayers(tileLayerList: Tile[]) {
    return { tileLayerList: tileLayerList };
  }

  // Stores the map to vuex
  @MutationAction
  async loadMap(newMap: Map): Promise<any> {
    return { map: newMap as Map };
  }

  // Updates Visibliity of a Layer
  @Action
  async updateVisibility(params: { layerId: string; visible: boolean }) {
    let tileLayerList = (await this.getMap.getLayers().getArray()) as Tile[];
    for (let i = 0; i < tileLayerList.length; i++) {
      if (params.layerId == tileLayerList[i].getProperties().id) {
        tileLayerList[i].setVisible(params.visible);
        break;
      }
    }
    for (let i = 0; i < this.vectorLayerList.length; i++) {
      if (params.layerId == this.vectorLayerList[i].getProperties().id) {
        this.vectorLayerList[i].setVisible(params.visible);
        break;
      }
    }
  }

  // Updates Visibliity of a Layer
  @Action
  async getVisibility(layerId: string) {
    let tileLayerList = (await this.getMap.getLayers().getArray()) as Tile[];
    for (let i = 0; i < tileLayerList.length; i++) {
      if (layerId == tileLayerList[i].getProperties().id) {
        return tileLayerList[i].getVisible();
      }
    }
  }

  // Removes length or area popup
  @Action
  removeMeasureToolTip() {
    let measureTooltipElement = document.getElementById("measureTooltip");
    if (measureTooltipElement != null) {
      let parentNode = measureTooltipElement.parentNode as any;
      parentNode.removeChild(measureTooltipElement);
    }
  }

  // Returns null or respective layer
  @Action
  getLayer(layerID: string) {
    const layers = this.getMap.getLayers().getArray();
    let l = layers[0] as Tile;
    for (const layer of layers) {
      if (layer.get("id") === layerID) {
        l = layer as Tile;
        return l;
      }
    }
    return null;
  }

  @MutationAction
  async initializeVectorLayer() {
    let tempList: any[] = [];

    let strokeStyle = new Style({
      fill: new Fill({
        color: "rgba(255, 255, 255, 0.2)"
      }),
      stroke: new Stroke({
        color: "#ffcc33",
        width: 2
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: "#ffcc33"
        })
      })
    });

    let problemStrokeStyle = new Style({
      fill: new Fill({
        color: "rgba(255, 255, 255, 0.2)"
      }),
      stroke: new Stroke({
        color: "#ff3300",
        width: 2
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: "#ff3300"
        })
      })
    });

    let drawStyle = new Style({
      fill: new Fill({
        color: "rgba(255, 255, 255, 0.2)"
      }),
      stroke: new Stroke({
        color: "#00ccff",
        width: 2
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: "#ffcc33"
        })
      })
    });

    // Drawing Layer
    let drawLayer = new VectorLayer({
      source: new VectorSource(),
      style: drawStyle
    });
    drawLayer.set("id", "draw");
    drawLayer.set("title", "Drawing Layer");
    tempList.push(drawLayer);

    // Problem Layer
    let problemLayer = new VectorLayer({
      source: new VectorSource(),
      style: problemStrokeStyle
    });
    problemLayer.set("id", "problem");
    problemLayer.set("title", "Problem Layer");
    tempList.push(problemLayer);
    return { vectorLayerList: tempList as VectorLayer[] };
  }
  // Zooms to subwatershed
  @Action
  async focusSubWatershed(params: any) {
    const ext = (await API.Extent(params)) as Focus;
    if (!this.MapLoaded) {
      await new Promise(resolve => setTimeout(resolve, 500));
    }
    let map = await this.map;
    map.getView().fit(ext.extent);
    if (ext.swid !== undefined) {
      const layer = (await this.getLayer("subwatershed")) as Tile;
      if (layer != null) {
        const layerSource = (await layer.getSource()) as TileWMS;
        layerSource.updateParams({
          VERSION: layerSource.getParams()["VERSION"],
          LAYERS: layerSource.getParams()["LAYERS"],
          CQL_FILTER: "swid>0;swid=" + ext.swid,
          STYLES: "allsubwatershed,selectedwatershed"
        });
      }
    }
    // debugger;
    let bounndary = new GeoJSON()
      .readFeatures(ext.geodata, {
        dataProjection: this.getMap.getView().getProjection(),
        featureProjection: this.getMap.getView().getProjection()
      })[0]
      .getGeometry();
    this.setCurrentSWBoundary(bounndary);
  }

  // Zooms to extent
  @Action
  async focusExtent(extent: [number, number, number, number]) {
    if (!this.MapLoaded) {
      await new Promise(resolve => setTimeout(resolve, 500));
    }
    let map = await this.map;
    map.getView().fit(extent);
  }

  // Zooms to subwatershed
  @Action
  async focusDistrict(params: {
    extent: [number, number, number, number];
    fid: any;
    layerName: string;
  }) {
    if (!this.MapLoaded) {
      await new Promise(resolve => setTimeout(resolve, 500));
    }
    let map = await this.map;
    map.getView().fit(params.extent);
    if (params.fid !== undefined) {
      let layerList = ["AndhraPradesh", "Maharashtra"];
      for (let layerName of layerList.filter(x => x != params.layerName)) {
        const layer = (await this.getLayer(layerName)) as Tile;
        if (layer != null) {
          const layerSource = (await layer.getSource()) as TileWMS;
          layerSource.updateParams({
            VERSION: layerSource.getParams()["VERSION"],
            LAYERS: layerSource.getParams()["LAYERS"],
            CQL_FILTER: "fid_1>0;fid_1=" + params.fid,
            STYLES: "iwmi_district,iwmi_district"
          });
        }
      }
      const layer = (await this.getLayer(params.layerName)) as Tile;
      if (layer != null) {
        const layerSource = (await layer.getSource()) as TileWMS;
        layerSource.updateParams({
          VERSION: layerSource.getParams()["VERSION"],
          LAYERS: layerSource.getParams()["LAYERS"],
          CQL_FILTER: "fid_1>0;fid_1=" + params.fid,
          STYLES: "iwmi_district,iwmi_districtselected"
        });
      }
    }
    // debugger;
    // let bounndary = new GeoJSON()
    //   .readFeatures(ext.geodata, {
    //     dataProjection: this.getMap.getView().getProjection(),
    //     featureProjection: this.getMap.getView().getProjection()
    //   })[0]
    //   .getGeometry();
    // this.setCurrentSWBoundary(bounndary);
  }

  // Zooms to subwatershed
  @Action
  async focusProvince(params: any) {
    const ext = (await API.Extent(params)) as Focus;
    if (!this.MapLoaded) {
      await new Promise(resolve => setTimeout(resolve, 500));
    }
    let map = await this.map;
    map.getView().fit(ext.extent);
    if (ext.swid !== undefined) {
      const layer = (await this.getLayer("subwatershed")) as Tile;
      if (layer != null) {
        const layerSource = (await layer.getSource()) as TileWMS;
        layerSource.updateParams({
          VERSION: layerSource.getParams()["VERSION"],
          LAYERS: layerSource.getParams()["LAYERS"],
          CQL_FILTER: "swid>0;swid=" + ext.swid,
          STYLES: "allsubwatershed,selectedwatershed"
        });
      }
    }
  }

  // Adds previously drawn problem
  @Action
  async setDraw() {
    this.updateVisibility({ layerId: "problem", visible: false });
    this.updateVisibility({ layerId: "draw", visible: true });
    let wkt = new WKT();
    let vectorLayer: VectorLayer = new VectorLayer();
    let wktData = problemStore.Problem.map;

    let vector: Tile | VectorLayer | null = await this.getLayer("draw");
    // if (vector != null) {
    //   this.getMap.removeLayer(vector);
    // }
    if (wktData != "" && wktData != null) {
      wktData = wktData.split(";")[1];
      var feature = wkt.readFeature(wktData, {
        dataProjection: this.getMap.getView().getProjection(),
        featureProjection: this.getMap.getView().getProjection()
      });
      let vectorSource = new VectorSource({
        features: [feature]
      });
      if (vector == null) {
        vectorLayer = new VectorLayer({
          source: vectorSource,
          style: this.strokeStyle
        });
        vectorLayer.set("id", "draw");
        vectorLayer.set("title", "Drawing Layer");
      } else {
        vector.setSource(vectorSource);
        vector.getSource().refresh();
      }
      // this.getMap.addLayer(vectorLayer);
    }
  }

  // Removes previously drawn drawings
  @Action
  async clearDraw() {
    this.getMap.removeInteraction(this.draw);
    this.getMap.removeInteraction(this.snap);
    let layer = await this.getLayer("draw");
    if (layer !== null) {
      layer.setSource(new VectorSource());
      layer.getSource().refresh();
    }
    let overlay = this.getMap.getOverlayById("measureTool");
    if (layer !== null) {
      this.getMap.removeOverlay(overlay);
    }
    problemStore.setArea(null);
    problemStore.setLength(null);
    this.getMap.un("pointermove", this.canDraw);
  }

  get MapLoaded() {
    return this.isMapLoaded;
  }

  // Adds Previously drawn problems from given list
  @Action
  async addProblem(problemList: ProblemModel[]) {
    this.updateVisibility({ layerId: "draw", visible: false });
    this.updateVisibility({ layerId: "problem", visible: true });
    if (!this.MapLoaded) {
      await new Promise(resolve => setTimeout(resolve, 1000));
    }
    var feature = [];
    let wkt = new WKT();
    this.removeMeasureToolTip();
    let vector: Tile | VectorLayer | null = await this.getLayer("problem");
    // if (vector != null) {
    //   this.getMap.removeLayer(vector);
    // }
    for (const problem of problemList) {
      let wktData = problem.map;
      if (wktData != "" && wktData != null) {
        wktData = wktData.split(";")[1];
        feature.push(
          wkt.readFeature(wktData, {
            dataProjection: this.getMap.getView().getProjection(),
            featureProjection: this.getMap.getView().getProjection()
          })
        );
      }
    }

    let vectorLayer = new VectorLayer();
    let vectorSource = new VectorSource({
      features: feature
    });
    if (vector == null) {
      // console.log("NULL");
      vectorLayer = new VectorLayer({
        source: vectorSource,
        style: this.strokeStyle
      });
      vectorLayer.set("id", "problem");
      vectorLayer.set("title", "Problem Layer");
    } else {
      vector.setSource(vectorSource);
      vector.getSource().refresh();
    }
    // let vectorLayer = new VectorLayer({
    //   source: new VectorSource({
    //     features: feature
    //   }),
    //   style: this.strokeStyle
    // });
    // vectorLayer.set("id", "problem");
    // vectorLayer.set("title", "Problem Layer");
    // this.getMap.addLayer(vectorLayer);
  }

  draw = new Draw({ type: GeometryType.POINT });
  snap = new Snap({});

  @Mutation
  setDrawing(draw: Draw) {
    this.draw = draw;
  }

  @Mutation
  setSnap(snap: Snap) {
    this.snap = snap;
  }

  get canDraw() {
    return (evt: any): any => {
      var isInsideThePolygon: any = true;
      var polyGeometry = this.currentSWBoundary;
      var zoomLevel: boolean = true;

      if (evt.dragging) {
        return;
      }
      isInsideThePolygon = polyGeometry.intersectsCoordinate(evt.coordinate);
      let zoom = this.getMap.getView().getZoom();
      if (zoom === undefined) {
        zoom = 6;
      }

      if (zoom < 15) {
        zoomLevel = false;
      }
      let allowDrawing = isInsideThePolygon && zoomLevel;
      let output = "";
      if (!zoomLevel) {
        output = "Please zoom more";
      } else if (!isInsideThePolygon) {
        output = "Please draw inside the boundary";
      }

      let measureTooltipElement = document.getElementById("measureTooltip");
      if (measureTooltipElement != null) {
        let parentNode = measureTooltipElement.parentNode as any;
        parentNode.removeChild(measureTooltipElement);
      }
      if (output !== "") {
        let measureCoordinate = evt.coordinate;
        measureTooltipElement = document.createElement("div");
        measureTooltipElement.className = "tooltip tooltip-static";
        measureTooltipElement.id = "measureTooltip";
        measureTooltipElement.innerHTML = output;
        let measureTooltip = new Overlay({
          id: "measureTool",
          element: measureTooltipElement,
          offset: [0, -15],
          positioning: "bottom-center" as OverlayPositioning
        });
        measureTooltip.setPosition(measureCoordinate);
        this.getMap.addOverlay(measureTooltip);
      }

      return allowDrawing;
    };
  }

  // Adds functionalnity to draw in map
  @Action
  async addDraw(typeSelect: string) {
    let vectorLayer = new VectorLayer();
    let vectorSource = new VectorSource();

    this.getMap.on("pointermove", this.canDraw);
    let wkt = new WKT();

    this.removeMeasureToolTip();

    this.getMap.removeInteraction(this.draw);
    this.getMap.removeInteraction(this.snap);
    let type: GeometryType | null = typeSelect as GeometryType;
    let vector: Tile | VectorLayer | null = await this.getLayer("draw");
    if (vector == null) {
      // console.log("NULL");
      vectorLayer = new VectorLayer({
        source: vectorSource,
        style: this.strokeStyle
      });
      vectorLayer.set("id", "draw");
      vectorLayer.set("title", "Drawing Layer");
    } else {
      vector.setSource(vectorSource);
      vector.getSource().refresh();
    }

    this.getMap.addLayer(vectorLayer);

    this.setDrawing(
      new Draw({
        condition: this.canDraw,
        source: vectorSource,
        type: type
      })
    );

    this.setSnap(new Snap({ source: vectorSource }));
    this.getMap.addInteraction(this.draw);
    this.getMap.addInteraction(this.snap);

    this.draw.on("drawend", async (event: any) => {
      setTimeout(() => {
        this.getMap.removeInteraction(this.draw);
        this.getMap.removeInteraction(this.snap);
      }, 1);
      type = null;
      const wktData = wkt.writeFeature(event.feature, {
        dataProjection: this.getMap.getView().getProjection(),
        featureProjection: this.getMap.getView().getProjection()
      });
      problemStore.setMap(wktData);

      let transformedWKT = event.feature
        .getGeometry()
        .transform("EPSG:4326", "EPSG:3857");

      let output = "";
      let measureCoordinate: any;

      if (transformedWKT instanceof Polygon) {
        let area = transformedWKT.getArea();

        event.feature.getGeometry().transform("EPSG:3857", "EPSG:4326");
        measureCoordinate = transformedWKT.getInteriorPoint().getCoordinates();
        if (area > 10000) {
          output =
            Math.round((area / 1000000) * 100) / 100 + " " + "km<sup>2</sup>";
        } else {
          output = Math.round(area * 100) / 100 + " " + "m<sup>2</sup>";
        }
        problemStore.setLength(null);
        problemStore.setArea(area);
      } else if (transformedWKT instanceof LineString) {
        let length = transformedWKT.getLength();

        event.feature.getGeometry().transform("EPSG:3857", "EPSG:4326");
        measureCoordinate = transformedWKT.getLastCoordinate();
        if (length > 100) {
          output = Math.round((length / 1000) * 100) / 100 + " " + "km";
        } else {
          output = Math.round(length * 100) / 100 + " " + "m";
        }
        problemStore.setArea(null);
        problemStore.setLength(length);
      } else {
        event.feature.getGeometry().transform("EPSG:3857", "EPSG:4326");
      }

      this.removeMeasureToolTip();

      let measureTooltipElement = document.createElement("div");
      measureTooltipElement.className = "tooltip tooltip-static";
      measureTooltipElement.id = "measureTooltip";
      measureTooltipElement.innerHTML = output;
      let measureTooltip = new Overlay({
        id: "measureTool",
        element: measureTooltipElement,
        offset: [0, -15],
        positioning: "bottom-center" as OverlayPositioning
      });
      measureTooltip.setPosition(measureCoordinate);
      this.getMap.addOverlay(measureTooltip);

      // Remove pointermove
      this.getMap.un("pointermove", this.canDraw);
    });

    return { draw: this.draw };
  }

  // Highlights the problemMap(WKT)
  @Action
  async highlightProblem(problemMap: string) {
    var feature = [];
    let wkt = new WKT();
    let strokeStyle = new Style({
      fill: new Fill({
        color: "rgba(255, 255, 255, 0.2)"
      }),
      stroke: new Stroke({
        color: "#ffcc33",
        width: 3
      }),
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({
          color: "#ffcc33"
        })
      })
    });
    let vector: Tile | VectorLayer | null = await this.getLayer(
      "vectorHighlight"
    );
    if (vector != null) {
      this.getMap.removeLayer(vector);
    }
    let wktData = problemMap;
    if (wktData != "" && wktData != null) {
      wktData = wktData.split(";")[1];
      feature.push(
        wkt.readFeature(wktData, {
          dataProjection: this.getMap.getView().getProjection(),
          featureProjection: this.getMap.getView().getProjection()
        })
      );
    }

    let vectorLayer = new VectorLayer({
      source: new VectorSource({
        features: feature
      }),
      style: strokeStyle
    });
    vectorLayer.set("id", "vectorHighlight");
    vectorLayer.set("title", "Vector Highlight Layer");
    this.getMap.addLayer(vectorLayer);
  }

  @Action
  async exportPng(params: { planid: number; picture: string }) {
    let result = new Promise(resolve => {
      this.getMap.once("rendercomplete", async (event: any) => {
        let canvas = event.context.canvas as HTMLCanvasElement;
        let blob: any = "";
        let file: File = {} as File;
        let dataURL = canvas.toDataURL();
        // DataURL to Blob
        let binary = atob(dataURL.split(",")[1]);
        let array = [];
        let i = 0;
        while (i < binary.length) {
          array.push(binary.charCodeAt(i));
          i++;
        }

        //Blob File
        blob = new Blob([new Uint8Array(array)], { type: "image/png" });
        file = new File([blob], params.planid + "_" + params.picture + ".png");
        let formData = new FormData();
        formData.append("plan", String(params.planid));
        formData.append(params.picture, file);
        // await axios
        //   .post("http://127.0.0.1:8000/api/gendoc/", formData, {
        //     headers: {
        //       "Content-Type": "multipart/form-data"
        //     }
        //   })
        //   .then(function () {
        //     console.log("SUCCESS!!");
        //     resolve("SUCCESS!!");
        //   })
        //   .catch(function () {
        //     console.log("FAILURE!!");
        //   });
        await API.uploadMapPicture(formData)
          .then(function() {
            // REQUIRED TO RESOLVE
            resolve("SUCCESS!!");
          })
          .catch(function() {
            console.log("FAILURE!!");
          });
      });
    });
    this.getMap.renderSync();
    return await result;
  }
}

export default getModule(MapModule);
