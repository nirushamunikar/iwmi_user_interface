import store from "@/store";
import {
  Module,
  VuexModule,
  Mutation,
  MutationAction,
  Action,
  getModule
} from "vuex-module-decorators";
import { ImageModel } from "@/store/models/DetailPlan/detailPlan";
import API from "@/store/API/image";

@Module({ name: "imageStore", dynamic: true, store })
class ImageModule extends VuexModule {
  ImageData: ImageModel[] = [];
  ImageResponse: any;

  // get imageName() {
  //   // console.log("printing " + this.ImageData.name);
  //   return this.ImageData;
  // }

  @Mutation
  pushImageData(image: ImageModel) {
    this.ImageData.push(image);
  }

  @Mutation
  async updateImageData(image: any) {
    if (image != undefined) {
      let index = 0;
      let id = this.ImageData.findIndex(x => x.id == image.id);
      if (id) {
        index = id;
      }
      console.log(index);
      this.ImageData.splice(index, 1, image);
    }
    console.log(this.ImageData);
  }

  @Mutation
  async saveImage() {
    // var obj = (this.state as ImageModule).ImageData;
    for (let data of this.ImageData) {
      await API.saveImage(data);
    }
    this.ImageData = [];
    // this.ImageResponse = obj;
    // console.log(obj);
    // return { ImageData: obj as ImageModel };
  }

  @Mutation
  async deleteImage(id: number) {
    if (id != 0) {
      await API.deleteImage(id);
    }
  }

  @Mutation
  async deleteImageFromArray(e: any) {
    for (let x of e) {
      if (e[x].file) {
        let id = this.ImageData.findIndex(t => t.id == e[x].id);
        this.ImageData.splice(id, 1);
      }
    }
  }
}

export default getModule(ImageModule);
