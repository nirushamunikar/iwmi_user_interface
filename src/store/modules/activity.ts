import store from "@/store";
import {
  Module,
  VuexModule,
  MutationAction,
  getModule
} from "vuex-module-decorators";
import { Activity } from "@/store/models/activity";
import API from "@/store/API/activity";

@Module({ name: "activityStore", dynamic: true, store })
class ActivityModule extends VuexModule {
  ActivityList: Activity[] = [];

  @MutationAction
  async getActivityByProblemId(problemId: number) {
    const obj = await API.getActivityByProblemId(problemId);
    return { ActivityList: obj as Activity[] };
  }
}

export default getModule(ActivityModule);
