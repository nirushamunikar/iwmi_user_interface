import store from "@/store";
import {
  Module,
  VuexModule,
  Mutation,
  MutationAction,
  Action,
  getModule
} from "vuex-module-decorators";
import {
  ProblemModel,
  ElementAffectedModel,
  ProblemCriteriaWithRank,
  ScopeOfProblem
} from "@/store/models/Problem/Problem";
import API from "@/store/API/Problem";

@Module({ name: "problemStore", dynamic: true, store })
class ProblemModule extends VuexModule {
  ProblemList: ProblemModel[] = [];

  Problem: ProblemModel = {
    id: 0,
    name: "",
    map: "",
    area: "",
    length: "",
    cause: "",
    severity: "",
    selected: false,
    planid: 0,
    problemserialdetail: "",
    elementaffected: [],
    scopeofproblems: []
  } as ProblemModel;

  SelectedProblemList: ProblemModel[] = [];

  @MutationAction
  async getProblemsByPlanId(planId: number) {
    const obj = await API.getProblemsByPlanId(planId);
    return { ProblemList: obj as ProblemModel[] };
  }

  @Action
  async getElementAffectedByProblemId(problemId: number) {
    const obj = await API.getElementAffectedListByProblemId(problemId);
    return obj as ElementAffectedModel[];
  }

  @Action
  async getScopeOfProblemByProblemId(problemId: number) {
    const obj = await API.getScopeOfProblemByProblemId(problemId);
    return obj as ScopeOfProblem[];
  }

  @MutationAction
  async saveProblem(problem: ProblemModel) {
    const obj = await API.saveProblem(problem);
    // obj["elementaffected"] = [];
    // obj["scopeofproblems"] = [];
    return { Problem: obj as ProblemModel };
  }

  @Action
  async deleteProblem(problemId: number) {
    const obj = await API.deleteProblem(problemId);
  }

  @MutationAction
  async getProblemById(para: any) {
    const problemList = await API.getProblemsByPlanId(para.planId);
    // debugger
    for (const obj of problemList) {
      if (obj.id == para.problemId) {
        // obj["elementaffected"] = [];
        // obj["scopeofproblems"] = [];
        return { Problem: obj };
      }
    }
  }

  @Mutation
  newProblem() {
    this.Problem = {
      id: 0,
      name: "",
      map: "",
      area: "",
      length: "",
      cause: "",
      severity: "",
      selected: false,
      planid: 0,
      problemserialdetail: "",
      elementaffected: [],
      scopeofproblems: []
    } as ProblemModel;
  }

  @Mutation
  loadElementAffectedToStore(elementaffected: ElementAffectedModel[]) {
    this.Problem.elementaffected = elementaffected;
  }

  @Mutation
  loadScopeOfProblemToStore(scopeofproblems: ScopeOfProblem[]) {
    this.Problem.scopeofproblems = scopeofproblems;
  }

  @Mutation
  setMap(map: string) {
    this.Problem.map = map;
  }

  @Mutation
  setArea(area: any) {
    this.Problem.area = area;
  }

  @Mutation
  setLength(length: any) {
    this.Problem.length = length;
  }

  @Action
  async getProblemCriteriaByPlanId(planId: number) {
    const obj = await API.getProblemCriteriaListByPlanId(planId);
    return obj as ProblemCriteriaWithRank[];
  }

  @Action
  async saveProblemCriteria(problemcriterias: ProblemCriteriaWithRank[]) {
    const obj = await API.saveProblemCriteria(problemcriterias);
    return obj as ProblemCriteriaWithRank;
  }

  @Action
  async saveElementAffected(problem: ElementAffectedModel) {
    const obj = await API.saveElementAffected(problem);
    return obj as ElementAffectedModel;
  }

  @Mutation
  pushElementAffected(elementaffected: ElementAffectedModel) {
    if (!this.Problem.elementaffected) {
      this.Problem.elementaffected = [];
    }
    this.Problem.elementaffected.push(elementaffected);
  }

  @Mutation
  updateElementAffected(params: {
    elementaffected: ElementAffectedModel;
    index: number;
  }) {
    // this.Problem.elementaffected[params.index] = params.elementaffected;
    this.Problem.elementaffected.splice(
      params.index,
      1,
      params.elementaffected
    );
  }

  @Mutation
  pushScopeOfProblems(scopeofproblems: ScopeOfProblem) {
    if (!this.Problem.scopeofproblems) {
      this.Problem.scopeofproblems = [];
    }
    this.Problem.scopeofproblems.push(scopeofproblems);
  }

  @Mutation
  removeScopeOfProblems(index: number) {
    this.Problem.scopeofproblems.splice(index, 1);
  }

  // @Mutation
  // updateScopeOfProblems(
  //   index: number,
  //   activityId: number,
  //   scopeOfActivityId?: number
  // ) {
  //   this.Problem.scopeofproblems[index].activityid = activityId;
  //   if (scopeOfActivityId != undefined) {
  //     this.Problem.scopeofproblems[index].scopeofactivityid = scopeOfActivityId;
  //   }
  // }

  @Action
  async getActivityListByProblemId(problemId: number) {
    const obj = await API.getActivityListByProblemId(problemId);
    return obj as any;
  }

  @MutationAction
  async getSelectedProblemsByPlanId(planId: number) {
    const obj = await API.getSelectedProblemsByPlanId(planId);
    return { SelectedProblemList: obj as ProblemModel[] };
  }

  // @Action
  // async saveProblemWeightage(saveData: any[]) {
  //   let list = await API.saveProblemWeightage(saveData);
  // }
}

export default getModule(ProblemModule);
