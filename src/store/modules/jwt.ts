import store from "@/store";
import {
  Module,
  VuexModule,
  Mutation,
  MutationAction,
  Action,
  getModule
} from "vuex-module-decorators";
import API from "@/store/API/jwt";
import {
  userInfo,
  userCreate,
  userInRole,
  userDetail,
  changeUsername,
  changePassword,
  forgotPassword,
  resetPassword
} from "@/store/models/jwt";
import { setJWT, clearJWT } from "@/store/API/api";
import { Page } from "@/store/models/meta";

@Module({ name: "JWTStore", dynamic: true, store })
class JWTModule extends VuexModule {
  authUser: any;
  isAuthenticated: Boolean = false;
  acc: any = localStorage.getItem("access_token") || null;
  ref: any = "";
  regResponse: any = "";
  role: any = localStorage.getItem("role") || null;
  UserInRole: userInRole[] = [];
  count: number = 0;
  accessiblePlanList: number[] = [];

  User: userDetail = {} as userDetail;
  UserList: userDetail[] = [] as userDetail[];
  Page: Page = {} as Page;

  @MutationAction
  async register(payload: userCreate) {
    var obj = (this.state as JWTModule).regResponse;
    obj = await API.register(payload);
    return { regResponse: obj as any };
  }

  @MutationAction
  async getJWT(payload: userInfo) {
    var obj = (this.state as JWTModule).acc;
    obj = await API.getJWT(payload);
    localStorage.setItem("refresh_token", obj.refresh);
    // this.acc = obj.access;
    localStorage.setItem("access_token", obj.access);
    localStorage.setItem("role", obj.role);
    return { ref: obj.refresh as any };
  }

  @Action
  async getAccessToken() {
    const acc = await API.getAccessToken({
      refresh: localStorage.getItem("refresh_token")
    });
    localStorage.removeItem("access_token");
    localStorage.setItem("access_token", acc.access);
    setJWT();
    // this.acc = acc.access;
    return { acc: acc.access as any };
  }

  get token() {
    return this.acc;
  }

  @Mutation
  setAuthUser(params: { authUser: string; isAuthenticated: Boolean }) {
    this.authUser = params.authUser;
    this.isAuthenticated = params.isAuthenticated;
  }

  get loggedIn() {
    return localStorage.getItem("access_token") !== null;
  }

  @Mutation
  setAccessiblePlan(accessibleplanlist: number[]) {
    this.accessiblePlanList = accessibleplanlist;
  }

  @Mutation
  logOut() {
    this.acc = null;
    this.ref = null;
    this.role = null;
    this.isAuthenticated = false;
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("role");
    clearJWT();
    location.replace("/");
  }

  @MutationAction
  async getUserInRole() {
    var list = (this.state as JWTModule).UserInRole;
    if (list.length === 0) {
      list = await API.getUserInRole();
    }
    return { UserInRole: list as userInRole[] };
  }

  @Mutation
  async deleteUserInRole(params: {
    userId: number;
    roleId: number;
    index: number;
  }) {
    if (params.roleId !== 0 && params.userId !== 0) {
      await API.deleteUserInRole(params.userId, params.roleId);
    }
  }

  @MutationAction
  async searchUserInRole(params: {
    users: userInRole[];
    searchString?: any;
    page?: any;
  }) {
    API.saveUserInRole(params.users);
    let query: string = "/?";
    if (params.searchString && params.searchString != undefined) {
      query += "&search=" + params.searchString;
    }
    if (params.page && params.page != undefined) {
      query += "&paging=true&page=" + params.page;
    }

    const res = (await API.getUserInRole(query)) as Page;
    const list = res.results;
    // const list = await API.getUserInRole(query);
    const page = res as Page;
    return { UserInRole: list as userInRole[], Page: page };
  }

  // @Mutation
  // pushUserInRole(userinrole: userInRole[]) {
  //   for (const i of userinrole) {
  //     this.UserInRole.push(i);
  //   }
  // }

  @Mutation
  pushUserInRole(userinrole: any) {
    this.UserInRole.push(userinrole);
  }

  // @Mutation
  // pushUserInRoleTemp(params: { index: number; object: userInRole }) {
  //     for (const i of params.object['districtname']){
  //       this.UserInRole[params.index].districtname.push(i);
  //     }

  //     let previousDistrict = this.UserInRole[params.index].districtname;
  //     let uniqueDistrict = previousDistrict.filter((elem, i, arr) => {
  //       if (arr.indexOf(elem) === i) {
  //         return elem
  //       }
  //     });

  //     this.UserInRole[params.index].districtname=[];

  //     for (const i of uniqueDistrict){
  //       this.UserInRole[params.index].districtname.push(i);
  //     }
  //
  //     // console.log(ab.filter((el, i, ab) => i === ab.indexOf(el)));
  //     // console.log(this.UserInRole[params.index].districtname.push(a.filter((el, i, a) => i === a.indexOf(el))));
  // }

  @Mutation
  pushUserInRoleTemp(params: { index: number; object: userInRole }) {
    this.UserInRole[params.index].district = [];
    for (const i of params.object["district"]) {
      this.UserInRole[params.index].district.push(i);
    }
    // console.log(this.UserInRole[params.index].district);
  }

  @Mutation
  updateUserInRole(params: { userinrole: userInRole; index: number }) {
    this.UserInRole.splice(params.index, 1, params.userinrole);
  }

  @MutationAction
  async saveUserInRole(userinrole: userInRole[]) {
    var obj = (this.state as JWTModule).UserInRole;
    obj = await API.saveUserInRole(userinrole);
    return { UserInRole: obj as userInRole[] };
  }

  @Action
  async changeUsername(username: changeUsername) {
    const chusr = await API.changeUsername(username);
  }

  @Action
  async changePassword(passwords: changePassword) {
    const chpass = await API.changePassword(passwords);
  }

  @Action
  async forgotPassword(email: forgotPassword) {
    const repass = await API.forgotPassword(email);
  }

  @Action
  async resetPassword(data: resetPassword) {
    const conrepass = await API.resetPassword(data);
  }
}

export default getModule(JWTModule);
