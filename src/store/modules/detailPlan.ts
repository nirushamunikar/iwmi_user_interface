import store from "@/store";
import {
  Module,
  VuexModule,
  Mutation,
  MutationAction,
  Action,
  getModule
} from "vuex-module-decorators";
import {
  ActivityStakeholderModel,
  ScopeOfActivityDetailModel,
  Activity,
  ActivitySetting,
  Meta,
  ActivityVariable
} from "@/store/models/DetailPlan/detailPlan";
import { SocioEconomic } from "@/store/models/plan/watershedPlan";
import imageStore from "@/store/modules/image";
import API from "@/store/API/DetailPlan";
import { IdName } from "@/store/models/meta";

@Module({ name: "detailPlanStore", dynamic: true, store })
class DetailPlanModule extends VuexModule {
  ActivityStakeholder: ActivityStakeholderModel[] = [];
  StakeholderCategory: IdName[] = [];
  PossibleContribution: IdName[] = [];
  AdditionalInformation: Activity[] = [];
  ScopeOfActivityDetail: ScopeOfActivityDetailModel[] = [];
  ScopeOfActivity: ScopeOfActivityDetailModel = {} as ScopeOfActivityDetailModel;
  StakeholderList: IdName[] = [];
  tempActivityStakeholder: ActivityStakeholderModel[] = [];
  tempScopeOfActivityDetail: ScopeOfActivityDetailModel[] = [];

  @MutationAction
  async storeActivityDetailInformation(obj: any) {
    //planId:44,problemId:48,activityId:3
    const bio = await API.geActivityDetailInformation(
      obj.planId,
      obj.problemId,
      obj.activityId
    );
    return { AdditionalInformation: bio as Activity[] };
  }

  @Action
  async getActivityDetailInformation(obj: any) {
    //planId:44,problemId:48,activityId:3
    const bio = await API.geActivityDetailInformation(
      obj.planId,
      obj.problemId,
      obj.activityId
    );
    console.log(bio)
    return bio as Activity[];
  }

  @Mutation
  async updateAdditionalInformation(param: {
    data: any;
    index: number;
    dataIndex: number;
  }) {
    this.AdditionalInformation[param.index].Data.splice(
      param.dataIndex,
      1,
      param.data
    );
  }

  @Mutation
  async pushAdditionalInformation(param: { data: any; index: number }) {
    this.AdditionalInformation[param.index].Data.push(param.data);
  }

  @Action
  async getStakeholderListByProblemId(problemId: number) {
    const obj = await API.getStakeholderListByProblemId(problemId);
    return obj as any;
  }

  @MutationAction
  async getStakeholderCategory() {
    var list = (this.state as DetailPlanModule).StakeholderCategory;
    if (list.length === 0) {
      list = await API.getStakeholderCategory();
    }
    return { StakeholderCategory: list as IdName[] };
  }

  @MutationAction
  async getPossibleContribution() {
    var list = (this.state as DetailPlanModule).PossibleContribution;
    if (list.length === 0) {
      list = await API.getPossibleContribution();
    }
    return { PossibleContribution: list as IdName[] };
  }

  @Mutation
  pushActivityStakeholder(activitystakeholder: ActivityStakeholderModel) {
    this.ActivityStakeholder.push(activitystakeholder);
  }

  @Mutation
  updateActivityStakeholder(params: {
    activitystakeholder: ActivityStakeholderModel;
    index: number;
  }) {
    this.ActivityStakeholder.splice(
      params.index,
      1,
      params.activitystakeholder
    );
  }

  @MutationAction
  async saveActivityStakeholder(
    activitystakeholder: ActivityStakeholderModel[]
  ) {
    var obj = (this.state as DetailPlanModule).ActivityStakeholder;
    obj = await API.saveActivityStakeholder(activitystakeholder);
    return { ActivityStakeholder: obj as ActivityStakeholderModel[] };
  }

  @MutationAction
  async getActivityStakeholderListbyActivityId(obj: any) {
    var list = (this.state as DetailPlanModule).ActivityStakeholder;

    list = await API.getActivityStakeholderListbyActivityId(obj);

    return { ActivityStakeholder: list as ActivityStakeholderModel[] };
  }

  @Action
  async loadActivityStakeholderListbyActivityId(obj: any) {
    let list = await API.getActivityStakeholderListbyActivityId(obj);

    return list as ActivityStakeholderModel[];
  }

  @Mutation
  pushScopeOfActivityDetail(scopeofactivitydetail: ScopeOfActivityDetailModel) {
    this.ScopeOfActivityDetail.push(scopeofactivitydetail);
  }

  @Mutation
  updateScopeOfActivityDetail(params: {
    scopeofactivitydetail: ScopeOfActivityDetailModel;
    index: number;
  }) {
    this.ScopeOfActivityDetail.splice(
      params.index,
      1,
      params.scopeofactivitydetail
    );
  }

  @Action
  async saveScopeOfActivityDetail(
    scopeofactivitydetail: ScopeOfActivityDetailModel
  ) {
    // var obj = (this.state as DetailPlanModule).ScopeOfActivity;
    var obj = await API.saveScopeOfActivityDetail(scopeofactivitydetail);
    // return { ScopeOfActivity: obj as ScopeOfActivityDetailModel };
    // this.ScopeOfActivity = obj;
    return obj as ScopeOfActivityDetailModel;
  }

  @MutationAction
  async getScopeOfActivityDetailListbyActivityId(obj: any) {
    var list = (this.state as DetailPlanModule).ScopeOfActivityDetail;

    list = await API.getScopeOfActivityDetailListbyActivityId(obj);

    return { ScopeOfActivityDetail: list as ScopeOfActivityDetailModel[] };
  }

  @Action
  async loadScopeOfActivityDetailListbyActivityId(obj: any) {
    let list = await API.getScopeOfActivityDetailListbyActivityId(obj);

    return list as ScopeOfActivityDetailModel[];
  }

  @MutationAction
  async saveAdditionalInformation(obj: any) {
    var data = (obj.socio as Activity[]).map(x => x.Data);

    var res = await API.saveAdditionalInformation(data);
    return {
      AdditionalInformation: res as Activity[]
    };
  }

  @Action
  async saveActivityInformationSetting(obj: any) {
    var ls: ActivitySetting[] = [];
    for (var i = 0; i < obj.socio.length; i++) {
      var meta = obj.socio[i].Meta as Meta[];
      for (var j = 0; j < meta.length; j++) {
        let id: number | undefined = 0;
        if (meta[j].defaultvisibleid !== undefined) {
          id = meta[j].defaultvisibleid;
        }
        const socioSetting: ActivitySetting = {
          planid: obj.planId,
          activityinformationvariableid: meta[j].id,
          visible: meta[j].defaultvisible,
          id: id
        };
        ls.push(socioSetting);
      }
    }
    var bio = API.saveActivityInformationSetting(ls);
    return { AdditionalInformation: obj.socio as Activity[] };
  }

  @Action
  async deleteStakeholderDetail(stakeholderId: number) {
    const obj = await API.deleteStakeholderDetail(stakeholderId);
  }

  @Action
  async deleteScopeOfActivityDetail(SOAId: number) {
    const obj = await API.deleteScopeOfActivityDetail(SOAId);
  }

  @Mutation
  async deleteAdditionalInformation(params: {
    planId: number;
    problemId: number;
    activityId: number;
    activitygroupId: number;
    rowindex: number;
    data: any;
  }) {
    const obj = await API.deleteAdditionalInformation(
      params.planId,
      params.problemId,
      params.activityId,
      params.activitygroupId,
      params.rowindex
    );
    this.AdditionalInformation[params.data.parentIndex].Data.splice(
      params.data.dataIndex,
      1
    );
  }

  @Action
  async getActivityDetailFromODK(obj: any) {
    //planId:44,problemId:48,activityId:3
    const bio = await API.getActivityDetailFromODK(obj.problemId);
    return bio as Activity[];
  }
}

export default getModule(DetailPlanModule);
