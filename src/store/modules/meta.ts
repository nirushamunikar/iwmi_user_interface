import store from "@/store";
import {
  Module,
  VuexModule,
  Mutation,
  MutationAction,
  Action,
  getModule
} from "vuex-module-decorators";
import API from "@/store/API/meta";
import {
  Province,
  District,
  Palika,
  IdName,
  Activity,
  ScopeOfActivity,
  Criteria,
  ProblemSubTypesModel,
  ObjectiveGroup,
  Objective,
  Role,
  User,
  Page
} from "@/store/models/meta";
import {
  PlanStakeholderModel,
  WatershedPlan
} from "@/store/models/plan/watershedPlan";
import APII from "@/store/API/Problem";

@Module({ name: "meta", dynamic: true, store })
class MetaModule extends VuexModule {
  public Watersheds: WatershedPlan[] = [];
  public SubWatersheds: WatershedPlan[] = [];
  public Province: Province[] = [];
  public Districts: District[] = [];
  public Palikas: Palika[] = [];
  public Programs: IdName[] = [];
  public ProgramTypes: IdName[] = [];
  public ProblemSubTypes: ProblemSubTypesModel[] = [];
  public Activities: Activity[] = [];
  public ScopeOfActivities: ScopeOfActivity[] = [];
  public Criterias: Criteria[] = [];
  public Stakeholders: IdName[] = [];
  public groupedObjective: (ObjectiveGroup | Objective)[] = [];
  public Users: User[] = [];
  public User: User = {} as User;
  public Roles: Role[] = [];
  public Page: Page = {} as Page;

  @MutationAction
  async loadStakeholders() {
    var stakeHolder = (this.state as MetaModule).Stakeholders;
    if (stakeHolder.length == 0) {
      //if watershed list is empty then load from api
      stakeHolder = await API.StakeholderList();
    }
    return { Stakeholders: stakeHolder as IdName[] };
  }

  @MutationAction
  async loadWatersheds(districtId: string) {
    var waterShed = (this.state as MetaModule).Watersheds;
    if (waterShed.length == 0) {
      //if watershed list is empty then load from api
      waterShed = await API.WatershedList();
    }
    return { Watersheds: waterShed as WatershedPlan[] };
  }

  @MutationAction
  async loadSubWatersheds(districtId: string) {
    var waterShed = (this.state as MetaModule).SubWatersheds;
    if (waterShed.length == 0) {
      //if watershed list is empty then load from api
      waterShed = await API.SubWatershedList();
    }
    return { SubWatersheds: waterShed as WatershedPlan[] };
  }

  @MutationAction
  async loadProvince() {
    var provinceListing = (this.state as MetaModule).Province;

    if (provinceListing.length === 0) {
      provinceListing = await API.Provincelisting();
    }
    return { Province: provinceListing as Province[] };
  }

  @MutationAction
  async loadDisticts() {
    var districtListing = (this.state as MetaModule).Districts;
    if (districtListing.length === 0) {
      districtListing = await API.Districtlisting();
    }
    return { Districts: districtListing as District[] };
  }

  @MutationAction
  async loadPalikas() {
    var palikaList = (this.state as MetaModule).Palikas;
    if (palikaList.length === 0) {
      palikaList = await API.PalikaList();
    }
    return { Palikas: palikaList as Palika[] };
  }

  // Related to Programs
  // Start
  @MutationAction
  async loadPrograms(forced: boolean = false) {
    var list = (this.state as MetaModule).Programs;
    if (forced || list.length === 0) {
      list = await API.Programs();
    }
    return { Programs: list as IdName[] };
  }

  @Mutation
  pushPrograms(program: IdName) {
    this.Programs.push(program);
  }

  @Mutation
  updateProgramList(param: { program: IdName; index: number }) {
    this.Programs.splice(param.index, 1, param.program);
  }

  @MutationAction
  async savePrograms(programs: IdName[]) {
    const list = await API.savePrograms(programs);
    return { Programs: list as IdName[] };
  }

  @Action
  async deleteProgram(programId: number) {
    const obj = await API.deleteProgram(programId);
  }

  @MutationAction
  async searchPrograms(params: {
    programs: IdName[];
    searchString?: any;
    page?: any;
  }) {
    API.savePrograms(params.programs);
    let query = "/";
    if (params.searchString || params.page) {
      query += "?";
    }
    if (params.searchString) {
      query += "&search=" + params.searchString;
    }
    if (params.page) {
      query += "&paging=true&page=" + params.page;
    }
    const res = (await API.Programs(query)) as Page;
    const list = res.results;
    return { Programs: list as IdName[], Page: res as Page };
  }
  // End

  @MutationAction
  async loadProgramTypes() {
    var list = (this.state as MetaModule).ProgramTypes;
    if (list.length === 0) {
      list = await API.ProgramTypes();
    }
    return { ProgramTypes: list as IdName[] };
  }

  @MutationAction
  async loadProblemSubTypes() {
    var list = (this.state as MetaModule).ProblemSubTypes;
    if (list.length === 0) {
      list = await API.ProblemSubTypes();
    }
    return { ProblemSubTypes: list as ProblemSubTypesModel[] };
  }

  // Related to Activities
  //Start
  @MutationAction
  async loadActivities(forced: boolean = false) {
    var list = (this.state as MetaModule).Activities;
    if (forced || list.length === 0) {
      list = await API.Activities();
    }
    return { Activities: list as Activity[] };
  }

  @Mutation
  pushActivity(activity: Activity) {
    this.Activities.push(activity);
  }

  @Mutation
  updateActivityList(param: { activity: Activity; index: number }) {
    this.Activities.splice(param.index, 1, param.activity);
  }

  @MutationAction
  async saveActivities(activity: Activity[]) {
    const list = await API.saveActivities(activity);
    return { Activities: list as Activity[] };
  }

  @Action
  async deleteActivity(activityId: number) {
    const obj = await API.deleteActivity(activityId);
  }

  @MutationAction
  async searchActivities(params: {
    activities: Activity[];
    searchString?: any;
    page?: any;
  }) {
    API.saveActivities(params.activities);
    let query = "/";
    if (params.searchString || params.page) {
      query += "?";
    }
    if (params.searchString) {
      query += "&search=" + params.searchString;
    }
    if (params.page) {
      query += "&paging=true&page=" + params.page;
    }
    const res = (await API.Activities(query)) as Page;
    const list = res.results;
    return { Activities: list as Activity[], Page: res };
  }
  //End

  // Related to Activities
  //Start
  @MutationAction
  async loadScopeOfActivities(noPaging: boolean = true) {
    let list: ScopeOfActivity[] = [];
    let page: Page = {} as Page;
    if (noPaging) {
      list = await API.ScopeOfActivities();
    } else {
      let pagedSOA = await API.ScopeOfActivities("/?no_paging=false");
      list = pagedSOA.results;
      page = pagedSOA;
    }
    return { ScopeOfActivities: list as ScopeOfActivity[], Page: page as Page };
  }

  @Mutation
  pushScopeOfActivity(activity: ScopeOfActivity) {
    this.ScopeOfActivities.push(activity);
  }

  @Mutation
  updateScopeOfActivityList(param: {
    activity: ScopeOfActivity;
    index: number;
  }) {
    this.ScopeOfActivities.splice(param.index, 1, param.activity);
  }

  @MutationAction
  async saveScopeOfActivities(activity: ScopeOfActivity[]) {
    const list = await API.saveScopeOfActivities(activity);
    return { ScopeOfActivities: list as ScopeOfActivity[] };
  }

  @Action
  async deleteScopeOfActivity(soaId: number) {
    const obj = await API.deleteScopeOfActivity(soaId);
  }

  @MutationAction
  async searchScopeOfActivities(params: {
    soa: ScopeOfActivity[];
    searchString?: any;
    page?: any;
  }) {
    API.saveScopeOfActivities(params.soa);
    let query: string = "/?";
    if (params.searchString && params.searchString != undefined) {
      query += "&search=" + params.searchString;
    }
    if (params.page && params.page != undefined) {
      query += "&page=" + params.page;
    }

    const res = (await API.ScopeOfActivities(query)) as Page;
    const list = res.results;
    const page = res as Page;
    return { ScopeOfActivities: list as ScopeOfActivity[], Page: page as Page };
  }
  //End

  // TODO: Merge Here
  // @MutationAction
  // async saveScopeOfActivities(obj: ScopeOfActivity[]) {
  //   var list = (this.state as MetaModule).ScopeOfActivities;
  //   for (const i in obj) {
  //     list = await APII.saveScopeOfActivities(obj[i]);
  //   }
  //   console.log(list as ScopeOfActivity[]);
  //   return { ScopeOfActivities: list as ScopeOfActivity[] };
  // }
  // Till Here

  @MutationAction
  async loadCriterias(planid: number) {
    var list = (this.state as MetaModule).Criterias;
    if (list.length === 0) {
      list = await API.Criterias(planid);
    }
    return { Criterias: list as Criteria[] };
  }

  @MutationAction
  async saveProblemWeightage(obj: any) {
    let saveData = [];
    for (const cri of obj.cri) {
      let temp = {
        id: cri.problemweightageid,
        weightage: cri.weightage,
        selected: cri.selected,
        planid: obj.planid,
        criteriaid: cri.id
      };
      saveData.push(temp);
    }
    let list1 = await APII.saveProblemWeightage(saveData);
    let list = await API.Criterias(obj.planid);
    return { Criterias: list as Criteria[] };
  }

  // Related to Objectives
  // Start
  @MutationAction
  async loadObjectives(showAsList: boolean = false) {
    const objectives = await API.Objectives({ list: showAsList });
    return { groupedObjective: objectives };
  }

  @Mutation
  pushObjective(objective: Objective) {
    this.groupedObjective.push(objective);
  }

  @Mutation
  updateObjectivesList(param: { objective: Objective; index: number }) {
    this.groupedObjective.splice(param.index, 1, param.objective);
  }

  @MutationAction
  async saveObjectives(objective: (ObjectiveGroup | Objective)[]) {
    const list = await API.saveObjectives(objective);
    return { groupedObjective: list };
  }

  @Action
  async deleteObjective(objectiveId: number) {
    const obj = await API.deleteObjective(objectiveId);
  }

  @MutationAction
  async searchObjectives(params: {
    objectives: (Objective | ObjectiveGroup)[];
    searchString?: any;
    page?: any;
  }) {
    API.saveObjectives(params.objectives);
    let query: string = "";
    if (params.searchString && params.searchString != undefined) {
      query += "&search=" + params.searchString;
    }
    if (params.page && params.page != undefined) {
      query += "&page=" + params.page;
    }

    const res = (await API.Objectives({
      list: true,
      query: query
    })) as Page;
    const list = res.results;
    return { groupedObjective: list, Page: res };
  }
  //End

  // Related to Users
  // Start
  @MutationAction
  async loadUsers(forced: boolean = false) {
    var userListing = (this.state as MetaModule).Users;
    let page: Page = {} as Page;
    if (forced === true || userListing.length === 0) {
      page = await API.loadUsers();
      userListing = page.results;
    }
    return { Users: userListing as User[], Page: page };
  }

  @MutationAction
  async loadAllUsers() {
    let userListing = await API.loadUsers("/?paging=false");
    return { Users: userListing as User[] };
  }

  @MutationAction
  async saveUsers(userList: User[]) {
    const list = await API.saveUsers(userList);
    return { Users: list as User[] };
  }

  @Mutation
  updateUserList(param: { user: User; index: number }) {
    this.Users.splice(param.index, 1, param.user);
  }

  @Mutation
  pushUserList(user: User) {
    this.Users.push(user);
  }

  @Mutation
  async deleteCustomUser(params: { id: number; index: number }) {
    if (params.id !== 0) {
      const user = await API.deleteCustomUser(params.id);
    }
    this.Users.splice(params.index, 1);
    return true;
  }

  @MutationAction
  async searchUsers(params: { users: User[]; searchString?: any; page?: any }) {
    API.saveUsers(params.users);
    let query = "/";
    if (params.searchString || params.page) {
      query += "?";
    }
    if (params.searchString) {
      query += "&search=" + params.searchString;
    }
    if (params.page) {
      query += "&paging=true&page=" + params.page;
    }
    const res = (await API.loadUsers(query)) as Page;
    const list = res.results;
    return { Users: list as User[], Page: res as Page };
  }

  //End

  @MutationAction
  async loadRoles() {
    var roleListing = (this.state as MetaModule).Roles;
    if (roleListing.length === 0) {
      roleListing = await API.loadRoles();
    }
    return { Roles: roleListing as Role[] };
  }

  @Action
  getPalikaByDistrictID(id: number): any {
    const palikas = this.Palikas.filter(x => x.districtid === id);
    return palikas;
  }

  // @Action
  // async getScopeOfActivityByActivityID(id: number) {
  //   const scopeofactivity = await this.ScopeOfActivities.filter(
  //     x => x.activityid === id
  //   );
  //   return scopeofactivity;
  // }

  @MutationAction
  async getUser() {
    const user = await API.getUser();
    return { User: user as User };
  }
}

export default getModule(MetaModule);
