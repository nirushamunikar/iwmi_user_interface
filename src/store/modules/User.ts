
import store from '@/store';
import { Module, VuexModule, Mutation, MutationAction, Action, getModule } from 'vuex-module-decorators';
import API from '@/store/API/plan';
import { UserSubmit } from '@/store/models/Variable';
import { UserInfo } from '@/store/models/userInfo';


@Module({ name: 'userStore', dynamic: true, store })
//@Module({ namespaced: true, name: 'userStore' })
class UserModule extends VuexModule {

    public userInfo: UserInfo | null = null

    @Mutation
    info(d: UserInfo) {
        this.userInfo = d;
    }

    @Action({ commit: 'info', rawError: true })
    async login(userSubmit: UserSubmit) {
        //  try {
        const userInfos = await API.loginUser(userSubmit);
        //setJWT(user.token)
        // console.log(userInfos);
        return userInfos as UserInfo;
    }
}

export default getModule(UserModule);
