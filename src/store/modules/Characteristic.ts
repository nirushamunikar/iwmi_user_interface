import Vue from 'vue';
import Vuex from 'vuex';
import { Module, VuexModule, Mutation, Action, getModule } from 'vuex-module-decorators';
import store from '@/store';

@Module({ name: 'productStore', dynamic: true, store })
class ProductStore extends VuexModule {

  private username: string = 'prabin';
  private socioEcnomics: any;

  @Mutation
  public setUsername(value: string) {
    this.username = value;
  }

  get getName(): string {
    return this.username;
  }

  get socioEconomics() {
    const js = [{
      name: 'Population',
      meta: [
        { caption: 'Household', type: 'Text', regex: '', message: '', options: [] },
        { caption: 'Male', type: 'Text', regex: '', message: '', options: [] },

      ],
      data: [
        [{ id: 1, value: '20' }, { id: 1, value: '30' }],
        [{ id: 1, value: '20' }, { id: 1, value: '30' }],
        [{ id: 1, value: '20' }, { id: 1, value: '30' }],
        [{ id: 1, value: '20' }, { id: 1, value: '40' }]]

    },
    ];
    
    // for (var i = 0; i < js.length; i++) {
    //   var stateOfRow: any = {};
    //   for (var j = 0; j < js[i].data.length; j++) {
    //     stateOfRow[i + "_" + j] = false;
    //   }
    // }

    this.socioEcnomics = js
    return this.socioEcnomics;

  }
}

export default getModule(ProductStore);
