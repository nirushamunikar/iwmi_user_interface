import store from "@/store";
import {
  Module,
  VuexModule,
  Mutation,
  MutationAction,
  Action,
  getModule
} from "vuex-module-decorators";
import API from "@/store/API/overview";
import {
  Evaluation,
  Overview,
  WorkPlan,
  Profile
} from "@/store/models/overview";

@Module({ name: "overviewStore", dynamic: true, store })
class OverviewModule extends VuexModule {
  Plan: Overview = {
    plan: {
      id: 0,
      estimatedbudget: "",
      selectionreason: "",
      objectives: "",
      scopes: "",
      name: "",
      priorityrank: 0,
      priority: "",
      fromyear: "",
      toyear: "",
      watershedname: "",
      watershedid: 0,
      subwatershedname: "",
      subwatershedid: 0
    }
  } as Overview;
  WorkPlan: WorkPlan[] = [] as WorkPlan[];
  isGeneratingDocs: boolean = false;

  @Mutation
  setisGeneratingDocs(val: boolean) {
    this.isGeneratingDocs = val;
  }
  Evaluation: Evaluation[] = [] as Evaluation[];
  ActivityProfile: Profile = {} as Profile;

  @MutationAction
  async getSituationOverview(planId: number) {
    let list = await API.getSituationOverview(planId);
    return { Plan: list as Overview };
  }

  @MutationAction
  async getWorkPlan(planId: number) {
    let list = await API.getWorkPlan(planId);
    return { WorkPlan: list as any };
  }

  @MutationAction
  async getEvaluation(planId: number) {
    let list = await API.getEvaluation(planId);
    return { Evaluation: list as any };
  }

  @MutationAction
  async getActivityProfile(obj: any) {
    let list = await API.getActivityProfile(
      obj.planid,
      obj.problemid,
      obj.activityid
    );
    return { ActivityProfile: list as any };
  }

  @Action
  async genDoc(
    params: { planId: number; forced: boolean } = { planId: 0, forced: false }
  ) {
    this.setisGeneratingDocs(true);
    let list = await API.genDoc(params.planId, params.forced);
    this.setisGeneratingDocs(false);
    return list as any;
  }

  @Action
  async saveWorkPlan(saveData: any[]) {
    let list = await API.saveWorkPlan(saveData);
  }

  @Action
  async saveEvaluation(saveData: any[]) {
    let list = await API.saveEvaluation(saveData);
  }

  @MutationAction
  async saveActivityProfile(saveData: any) {
    let list = await API.saveActivityProfile(saveData);
    return { ActivityProfile: list as any };
  }

  @MutationAction
  public async saveActivityProfilePicture(saveData: any): Promise<any> {
    let list = await API.saveActivityProfilePicture(saveData);
    return { ActivityProfile: list as any };
  }

  @Action
  public async deleteActivityProfilePicture(id: number): Promise<any> {
    let list = await API.deleteActivityProfilePicture(id);
  }
}
export default getModule(OverviewModule);
