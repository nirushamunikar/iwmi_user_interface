import Vue from "vue";
import Vuex from "vuex";
import {
  Module,
  VuexModule,
  Mutation,
  Action,
  getModule,
  MutationAction
} from "vuex-module-decorators";
import store from "@/store";

@Module({ name: "commonstore", dynamic: true, store })
class CommonStore extends VuexModule {
  public isLoading: boolean = false;
  public planPercent: number = 0;
  public beforeDetailPath: string = "/Plan/List";
  public beforeOverviewPath: string = "/Plan/List";

  public beforeActivityStakeholderPath: string = "";
  public beforeScopeOfActivityPath: string = "";

  public beforeActivityList: string = "/Plan/List";
  public beforeReport: string = "/Plan/List";

  public beforeAdminSetting: string = "/Plan/List";
  public beforeAbout: string = "/Plan/List";
  public beforeProfile: string = "/Plan/List";

  public isActive: boolean = false;
  public buttonIsActive: boolean = false;

  @Mutation
  toggleIsActive() {
    this.isActive = !this.isActive;
  }

  @Mutation
  toggleButtonIsActive() {
    this.buttonIsActive = !this.buttonIsActive;
  }

  @Mutation
  setIsActive(active: boolean) {
    this.isActive = active;
  }

  @Mutation
  setPlanPercent(percent: number) {
    this.planPercent = percent;
  }

  @Mutation
  setBeforeDetailPath(path: string) {
    this.beforeDetailPath = path;
  }

  @Mutation
  setBeforeOverviewPath(path: string) {
    this.beforeOverviewPath = path;
  }

  @Mutation
  setBeforeActivityStakeholderPath(path: string) {
    this.beforeActivityStakeholderPath = path;
  }

  @Mutation
  setBeforeScopeOfActivityPath(path: string) {
    this.beforeScopeOfActivityPath = path;
  }

  @Mutation
  setBeforeActivityList(path: string) {
    this.beforeActivityList = path;
  }

  @Mutation
  setBeforeReport(path: string) {
    this.beforeReport = path;
  }

  @Mutation
  setBeforeAdminSetting(path: string) {
    this.beforeAdminSetting = path;
  }

  @Mutation
  setBeforeAbout(path: string) {
    this.beforeAbout = path;
  }

  @Mutation
  setBeforeProfile(path: string) {
    this.beforeProfile = path;
  }

  @Mutation
  setLoading(val: boolean) {
    this.isLoading = val;
  }

  @Action({ commit: "setLoading", rawError: true })
  showLoading() {
    return true;
  }

  @Action({ commit: "setLoading" })
  hideLoading() {
    return false;
  }
}

export default getModule(CommonStore);
