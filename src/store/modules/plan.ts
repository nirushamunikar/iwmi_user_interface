import store from "@/store";
import {
  Module,
  VuexModule,
  Mutation,
  MutationAction,
  Action,
  getModule
} from "vuex-module-decorators";
import API from "@/store/API/plan";
import {
  WatershedPlan,
  SocioVariable,
  BioVariable,
  BioPhysical,
  BioSetting,
  SocioEconomic,
  SocioEconomicSetting,
  Meta,
  SubWatershedDistrictPalika,
  PlanStakeholderModel,
  Search,
  wmpsPlan,
  CategoryBasedDisaster,
  ScenarioBasedManagementCondition,
  ContingencyPlan,
  ManagementConditionBasedManagementSubCondition,
  // PoultryContingencyPlan
} from "@/store/models/plan/watershedPlan";

@Module({ name: "planStore", dynamic: true, store })
//@Module({ namespaced: true, name:Z 'userStore' })
class PlanModule extends VuexModule {
  public watershedPlan: WatershedPlan = {} as WatershedPlan;
  public watershedPlanList: WatershedPlan[] = [];
  public subWaterShedDistrictPalikas: SubWatershedDistrictPalika[] = [];
  public PlanStakeholder: PlanStakeholderModel[] = [];
  public tempPlanStakeholder: PlanStakeholderModel[] = [];
  public bioPhysical: BioPhysical[] = [];
  public SocioList: SocioEconomic[] = [];
  public myWatershedPlanList: wmpsPlan = {} as wmpsPlan;
  public otherWatershedPlanList: wmpsPlan = {} as wmpsPlan;
  public categoryBasedDisaster: CategoryBasedDisaster[] = [];
  public scenarioBasedManagementCondition: ScenarioBasedManagementCondition[] = [];
  public ContingencyPlan: ContingencyPlan = {} as ContingencyPlan;
  public managementConditionBasedManagementSubCondition: ManagementConditionBasedManagementSubCondition[] = [];
  // public poultryContingencyPlan: PoultryContingencyPlan[] = [];

  get plans() {
    return this.watershedPlanList;
  }

  get pagePlans() {
    return this.myWatershedPlanList;
  }

  get pagePlansOther() {
    return this.otherWatershedPlanList;
  }

  get BioPhysicalList() {
    return this.bioPhysical;
  }

  @Action
  async getBioPhysicalByPlanId(planId: number) {
    const bio = await API.getBioPhysical(planId);
    return bio as BioPhysical[];
  }

  @MutationAction
  async getSocioEconomicByPlanId(planId: number) {
    const bio = await API.getSocioEconomic(planId);
    return { SocioList: bio as SocioEconomic[] };
  }

  @Mutation
  async pushSocioEconomic(param: {
    data: any;
    index: number;
  }) {
    this.SocioList[param.index].Data.push(param.data);
  }

  @Mutation
  async updateSocioEconomic(param: {
    data: any;
    index: number;
    dataIndex: number;
  }) {
    this.SocioList[param.index].Data.splice(param.dataIndex, 1, param.data);
  }

  @MutationAction
  async storeBioPhysicalByPlanId(planId: number) {
    (this.state as PlanModule).scenarioBasedManagementCondition = [] as ScenarioBasedManagementCondition[];
    (this.state as PlanModule).managementConditionBasedManagementSubCondition = [] as ManagementConditionBasedManagementSubCondition[];
    const bio = await API.getBioPhysical(planId);
    return { bioPhysical: bio as BioPhysical[] };
  }

  @Mutation
  async updateBioPhysical(param: { value: string; variableid: number }) {
    for (const bio of this.bioPhysical) {
      let changed = false;
      for (const meta of bio.Meta) {
        if (meta.id === param.variableid) {
          meta.data.value = param.value;
          changed = true;
          break;
        }
      }
      if (changed) {
        break;
      }
    }
    // console.log(biovar);
  }

  @MutationAction
  async savePlan(plan: WatershedPlan) {
    const planReq = await API.savePlan(plan);
    var p = planReq as WatershedPlan;
    return { watershedPlan: p };
  }

  @MutationAction
  async getSubWatershedById(id: number) {
    const result = await API.getSubwatershedById(id);
    return {
      subWaterShedDistrictPalikas: result as SubWatershedDistrictPalika[]
    };
  }

  @MutationAction
  async getPlanByPlanId(planId: number) {
    // var index = (this.state as PlanModule).watershedPlanList.findIndex(
    //   x => x.id === planId
    // );
    // var plan = {} as WatershedPlan;
    // if (index != -1) {
    //   plan = (this.state as PlanModule).watershedPlanList[index];
    // }
    //debugger
    const plan = await API.getPlanById(planId);
    // console.log(plan);
    return {
      watershedPlan: plan as WatershedPlan
    };
  }


  @MutationAction
  async getPlanListing(
    params: { search?: any; page?: number } = {
      page: 1
    }
  ) {
    const planReq = await API.getPlan({
      search: params.search,
      page: params.page
    });
    return { myWatershedPlanList: planReq as wmpsPlan };
  }

  @MutationAction
  async getOtherPlanListing(
    params: { search?: any; page?: number } = {
      page: 1
    }
  ) {
    const planReq = await API.getPlan({
      search: params.search,
      page: params.page,
      other: true
    });
    return { otherWatershedPlanList: planReq as wmpsPlan };
  }

  // @MutationAction
  // async getPlanListingBySearch(search: Search) {
  //   const searchPlan = await API.getPlanBySearch(search.search);
  //   return { watershedPlanList: searchPlan as WatershedPlan[] };
  // }

  // @MutationAction
  // async getPlanListingPage(page: number) {
  //   const planReq = await API.getPlanPage(page);
  //   return { myWatershedPlanList: planReq as wmpsPlan };
  // }

  // @MutationAction
  // async getPlanListingPageOther(page: number) {
  //   const planReq = await API.getPlanPageOther(page);
  //   return { otherWatershedPlanList: planReq as wmpsPlan };
  // }

  @MutationAction
  async getCharacteristics(planId: number) {
    var index = (this.state as PlanModule).watershedPlanList.findIndex(
      x => x.id === planId
    );
    var plan = {} as WatershedPlan;
    if (index != -1) {
      plan = (this.state as PlanModule).watershedPlanList[index];
      const bio = await API.getBioPhysical(planId);
      const socio = await API.getSocioEconomic(planId);
      (this.state as PlanModule).watershedPlanList[
        index
      ].socioeconomiclist = socio;
      (this.state as PlanModule).watershedPlanList[index].biophysicallist = bio;
    }
    return {
      watershedPlanList: (this.state as PlanModule).watershedPlanList,
      watershedPlan: (this.state as PlanModule).watershedPlanList[index]
    };
  }

  @MutationAction
  async saveBioPhysical(obj: any) {
    var index = (this.state as PlanModule).watershedPlanList.findIndex(
      x => x.id === obj.planId
    );
    if (index != -1) {
      (this.state as PlanModule).watershedPlanList[index].biophysicallist =
        obj.bio;
    }
    var ls: BioVariable[] = [];
    // cosnt b = obj.bio as met
    for (var i = 0; i < obj.bio.length; i++) {
      var meta = obj.bio[i].Meta as Meta[];
      for (var j = 0; j < meta.length; j++) {
        const bioVariable: BioVariable = {
          planid: obj.planId,
          value: meta[j].data.value,
          variableid: meta[j].id,
          id: meta[j].data.id
        };
        if (meta[j].data.value !== "") {
          ls.push(bioVariable);
        }
      }
    }
    var bio = await API.saveBioPhysical(ls);
    // console.log(bio);
    return { bioPhysical: bio as BioPhysical[] };
  }

  @MutationAction
  async saveBioPhysicalSetting(obj: any) {
    var ls: BioSetting[] = [];
    for (var i = 0; i < obj.bio.length; i++) {
      var meta = obj.bio[i].Meta as Meta[];
      for (var j = 0; j < meta.length; j++) {
        let id: number | undefined = 0;
        if (meta[j].defaultvisibleid !== undefined) {
          id = meta[j].defaultvisibleid;
        }
        const bioVariable: BioSetting = {
          planid: obj.planId,
          biophysicalvariableid: meta[j].id,
          visible: meta[j].defaultvisible,
          id: id
        };
        ls.push(bioVariable);
      }
    }
    var bio = API.saveBioPhysicalSetting(ls);
    return { bioPhysical: obj.bio as BioPhysical[] };
  }

  @MutationAction
  async saveSocioEconomics(obj: any) {
    // var index = (this.state as PlanModule).watershedPlanList.findIndex(
    //   x => x.id === obj.planId
    // );

    // if (index != -1) {
    //   (this.state as PlanModule).watershedPlanList[index].socioeconomiclist =
    //     obj.socio;
    // }
    var data = (obj.socio as SocioEconomic[]).map(x => x.Data);
    // debugger

    var res = await API.saveSocioEconomic(data);
    return { SocioList: res as SocioEconomic[] };

    //   console.log(s);
    // return {}
    //   watershedPlanList: (this.state as PlanModule).watershedPlanList,
    //   watershedPlan: (this.state as PlanModule).watershedPlanList[index]
    // };
  }

  @MutationAction
  async saveSocioEconomicSetting(obj: any) {
    var ls: SocioEconomicSetting[] = [];
    for (var i = 0; i < obj.socio.length; i++) {
      var meta = obj.socio[i].Meta as Meta[];
      for (var j = 0; j < meta.length; j++) {
        let id: number | undefined = 0;
        if (meta[j].defaultvisibleid !== undefined) {
          id = meta[j].defaultvisibleid;
        }
        const socioSetting: SocioEconomicSetting = {
          planid: obj.planId,
          socioeconomicvariableid: meta[j].id,
          visible: meta[j].defaultvisible,
          id: id
        };
        ls.push(socioSetting);
      }
    }
    var bio = API.saveSocioEconomicSetting(ls);
    return { SocioList: obj.socio as SocioEconomic[] };
  }

  @MutationAction
  async savePlanStakeholder(planstakeholder: PlanStakeholderModel[]) {
    var obj = (this.state as PlanModule).PlanStakeholder;
    obj = await API.savePlanStakeholder(planstakeholder);

    return { PlanStakeholder: obj as PlanStakeholderModel[] };
  }

  @MutationAction
  async getStakeholderListByPlanId(planId: number) {
    var list = (this.state as PlanModule).PlanStakeholder;
    list = await API.getStakeholderListByPlanId(planId);
    return { PlanStakeholder: list as PlanStakeholderModel[] };
  }

  @Mutation
  pushPlanStakeholder(planstakeholder: PlanStakeholderModel) {
    if (!this.PlanStakeholder) {
      this.PlanStakeholder = [];
    }
    this.PlanStakeholder.push(planstakeholder);
  }

  @Mutation
  updatePlanStakeholder(params: {
    planstakeholder: PlanStakeholderModel;
    index: number;
  }) {
    // var value = this.planStakeholder.filter(x => x.id === planstakeholder.id);
    // this.planStakeholder.push(planstakeholder);

    // return { planStakeholder: this.planStakeholder };
    this.PlanStakeholder.splice(params.index, 1, params.planstakeholder);
  }

  @Action
  updatePlanPercentage(data: { planid: number; percentCompleted: number }) {
    let obj = API.updatePlanPercentage({
      planid: data.planid,
      percentcompleted: data.percentCompleted
    });
    this.getPlanListing();
  }

  @Action
  async deleteStakeholder(stakeholderId: number) {
    const obj = await API.deleteStakeholder(stakeholderId);
  }

  @Mutation
  async deleteSocioEconomic(params: {
    planId: number;
    socioeconomicId: number;
    rowindex: number;
    data: any
  }) {
    const obj = await API.deleteSocioEconomic(
      params.planId,
      params.socioeconomicId,
      params.rowindex
    );
    this.SocioList[params.data.parentIndex].Data.splice(params.data.dataIndex, 1);
  }

  @Action
  async getSocioEconomicFeatures(planid: any) {
    const bio = await API.getSocioEconomicFeatures(
      planid
    );
    return bio as SocioEconomic[];
  }

  @MutationAction
  async getScenario(obj: any) {
    // (this.state as PlanModule).categoryBasedDisaster = [];
    (this.state as PlanModule).scenarioBasedManagementCondition = [] as ScenarioBasedManagementCondition[];
    (this.state as PlanModule).managementConditionBasedManagementSubCondition = [] as ManagementConditionBasedManagementSubCondition[];
    var scenario  = (this.state as PlanModule).categoryBasedDisaster;
    scenario = await API.getScenario(obj.district, obj.crop);
    return { categoryBasedDisaster: scenario as CategoryBasedDisaster[] };
  }

  @MutationAction
  async getSituation(obj: any) {
    var situation  = (this.state as PlanModule).scenarioBasedManagementCondition;
    situation = await API.getSituation(obj.district, obj.crop, obj.scenario);
    return { scenarioBasedManagementCondition: situation as ScenarioBasedManagementCondition[] };
  }

  @MutationAction
  async getSubCondition(obj: any) {
    var subcondition  = (this.state as PlanModule).managementConditionBasedManagementSubCondition;
    subcondition = await API.getSubCondition(obj.district, obj.crop, obj.scenario, obj.condition);
    return { managementConditionBasedManagementSubCondition: subcondition as ManagementConditionBasedManagementSubCondition[] };
  }

  @MutationAction
  async gethtml(obj: any) {
    var html  = (this.state as PlanModule).ContingencyPlan;
    html = await API.gethtml(obj.district, obj.crop, obj.scenario, obj.condition);
    return { ContingencyPlan: html as ContingencyPlan };
  }

  @MutationAction
  async gethtmlCropFisheries(obj: any) {
    var html  = (this.state as PlanModule).ContingencyPlan;
    html = await API.gethtmlCropFisheries(obj.district, obj.crop, obj.scenario, obj.condition, obj.subcondition);
    return { ContingencyPlan: html as ContingencyPlan };
  }

  // @MutationAction
  // async gethtmlPoultry(obj: any) {
  //   var html  = (this.state as PlanModule).poultryContingencyPlan;
  //   html = await API.gethtmlPoultry(obj.district, obj.crop, obj.scenario, obj.condition);
  //   console.log(html);
  //   return { poultryContingencyPlan: html as PoultryContingencyPlan[] };
  // }

}

export default getModule(PlanModule);
