// import axios from 'axios';
// import { UserSubmit } from '@/store/models/Variable'
// import Vue from 'vue'
// import { Action } from '@/store/actionType'
// import { WatershedPlan } from '@/store/models/plan/createPlanRequest.ts'

// const conduitApi = axios.create({
//     baseURL: Action.Base,
// });

// async function postApi(action: string, param: any): Promise<any> {
//     return await new Promise((resolve, reject) => {
//         conduitApi.post("/" + action, param).then(res => {
//             console.log(res)
//             resolve(res)
//         }, err => {
//             var vm = new Vue()
//             vm.$snotify.error(err.response.Message, "error");
//         })
//     });
// }

// async function getApi(action: string, param: string): Promise<any> {
//     return await new Promise((resolve, reject) => {
//         conduitApi.get("/" + action+"/").then(res => {
//             console.log(res)
//             resolve(res.data)
//         }, err => {
//             var vm = new Vue()
//             vm.$snotify.error(err.response.data.Message, "error");

//         })
//     });
// }

// function setJWT(jwt: string) {
//     conduitApi.defaults.headers.common['Authorization'] = `Token ${jwt}`;
// }

// function clearJWT() {
//     delete conduitApi.defaults.headers.common['Authorization'];
// }

// export default class API {
//     public static async  loginUser(user: UserSubmit): Promise<any> {
//         const response = await postApi(Action.Login, {
//             UserName: user.UserName, Password: user.Password, DeviceType: user.DeviceType
//         });
//         return response as any;
//     }

//     public static async  createPlan(plan: WatershedPlan): Promise<any> {
//         const response = await postApi(Action.Plan,plan);
//         return response as any;
//     }

//     public static async  getPlan(): Promise<any> {
//         const response = await getApi(Action.Plan,"");
//         return response as any;
//     }
// }
