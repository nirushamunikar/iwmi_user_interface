declare module '*.vue' {
  import Vue from 'vue';

  export default Vue;
}

declare module "vue-progressbar" {
  import { PluginFunction } from "vue";

  export const install: PluginFunction<{}>;

  interface ProgressMethods {
    start(): void;
    finish(): void;
    fail(): void;
    increase(num: number): void;
    decrease(num: number): void;
  }

  module "vue/types/vue" {
    interface Vue {
      $Progress: ProgressMethods;
    }
  }
}

declare module 'element-ui/lib/locale/lang/en' {
}